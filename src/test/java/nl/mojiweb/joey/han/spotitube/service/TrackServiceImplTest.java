package nl.mojiweb.joey.han.spotitube.service;

import nl.mojiweb.joey.han.spotitube.dto.TrackDTO;
import nl.mojiweb.joey.han.spotitube.exception.ResourceNotFoundException;
import nl.mojiweb.joey.han.spotitube.persistence.TrackDAO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TrackServiceImplTest {

    @Mock
    private PlaylistService playlistService;
    @Mock
    private TrackDAO trackDAO;

    @InjectMocks
    private TrackServiceImpl sut;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllTracksShouldCallTrackDAOGetTracks() {
        List<TrackDTO> tracks = new ArrayList<>();
        when(trackDAO.getTracks()).thenReturn(tracks);

        sut.getAllTracks();

        verify(trackDAO).getTracks();
    }

    @Test
    void getAllTracksShouldReturnAllTracks() {
        List<TrackDTO> tracks = new ArrayList<>();
        when(trackDAO.getTracks()).thenReturn(tracks);

        var actual = sut.getAllTracks();

        assertEquals(tracks, actual.getTracks());
    }

    @Test
    void getTracksByPlaylistShouldCallPlaylistService() {
        List<TrackDTO> tracks = new ArrayList<>();
        int existingPlaylistId = 1;
        when(trackDAO.getTracksByPlaylist(anyInt())).thenReturn(tracks);

        sut.getTracksByPlaylist(existingPlaylistId);

        verify(playlistService).getPlaylist(Mockito.anyInt());
    }

    @Test
    void getTracksByPlaylistShouldThrowResourceNotFoundException() {
        int nonExistingId = 1;
        when(playlistService.getPlaylist(Mockito.anyInt())).thenThrow(ResourceNotFoundException.class);

        assertThrows(ResourceNotFoundException.class,() -> sut.getTracksByPlaylist(nonExistingId));
    }

    @Test
    void removeTrackFromPlaylistShouldCallPlaylistService() {
        int existingId = 1;
        int trackId = 2;
        sut.removeTrackFromPlaylist(existingId, trackId);

        verify(playlistService).getPlaylist(existingId);
    }

    @Test
    void removeTrackFromPlaylistShouldCallTrackDAO() {
        int existingId = 1;
        int trackId = 2;
        sut.removeTrackFromPlaylist(existingId, trackId);

        verify(trackDAO).removeTrackFromPlaylist(existingId, trackId);

    }

    @Test
    void removeTrackFromPlaylistShouldThrowResourceNotFoundException() {
        int nonExistingId = 1;
        int trackId = 1;
        when(playlistService.getPlaylist(Mockito.anyInt())).thenThrow(ResourceNotFoundException.class);

        assertThrows(ResourceNotFoundException.class,() -> sut.removeTrackFromPlaylist(nonExistingId, trackId));
    }

    @Test
    void addTrackForPlaylistShouldCallPlaylistService() {
        int existingId = 1;
        TrackDTO trackDTO = new TrackDTO();
        sut.addTrackForPlaylist(existingId, trackDTO);

        verify(playlistService).getPlaylist(existingId);
    }

    @Test
    void addTrackForPlaylistShouldCallTrackDAO() {
        int existingId = 1;
        TrackDTO trackDTO = new TrackDTO();
        sut.addTrackForPlaylist(existingId, trackDTO);

        verify(trackDAO).addTrackToPlaylist(existingId, trackDTO);

    }

    @Test
    void addTrackForPlaylistShouldThrowResourceNotFoundException() {
        int nonExistingId = 1;
        TrackDTO trackDTO = new TrackDTO();
        when(playlistService.getPlaylist(Mockito.anyInt())).thenThrow(ResourceNotFoundException.class);

        assertThrows(ResourceNotFoundException.class,() -> sut.addTrackForPlaylist(nonExistingId, trackDTO));
    }

    @Test
    void getTracksExcludingPlaylistShouldReturnAllTracks() {
        List<TrackDTO> tracks = new ArrayList<>();
        int existingPlaylistId = 1;
        when(trackDAO.getTracksExcludingPlaylist(anyInt())).thenReturn(tracks);

        var actual = sut.getTracksExcludingPlaylist(existingPlaylistId);

        assertEquals(tracks, actual.getTracks());
    }

    @Test
    void getTracksExcludingPlaylistShouldNeverCallPlaylistService() {
        int existingId = 1;

        sut.getTracksExcludingPlaylist(existingId);

        verify(playlistService, never()).getPlaylist(Mockito.anyInt());
    }

    @Test
    void getTracksExcludingPlaylistShouldCallTrackDAO() {
        int existingId = 1;

        sut.getTracksExcludingPlaylist(existingId);

        verify(playlistService, never()).getPlaylist(Mockito.anyInt());
    }
}