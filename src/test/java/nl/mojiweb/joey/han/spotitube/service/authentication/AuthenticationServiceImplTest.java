package nl.mojiweb.joey.han.spotitube.service.authentication;

import nl.mojiweb.joey.han.spotitube.dto.UserDTO;
import nl.mojiweb.joey.han.spotitube.exception.UserNotFoundException;
import nl.mojiweb.joey.han.spotitube.persistence.UserDAO;
import nl.mojiweb.joey.han.spotitube.dto.LoginDTO;
import nl.mojiweb.joey.han.spotitube.dto.TokenDTO;
import nl.mojiweb.joey.han.spotitube.exception.InvalidCredentialsException;
import nl.mojiweb.joey.han.spotitube.exception.InvalidTokenException;
import nl.mojiweb.joey.han.spotitube.service.authentication.AuthenticationServiceImpl;
import nl.mojiweb.joey.han.spotitube.service.authentication.PasswordVerificationService;
import nl.mojiweb.joey.han.spotitube.service.authentication.TokenService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AuthenticationServiceImplTest {

    private static final String TEST_USER = "testUser";
    private static final String TEST_PASSWORD = "testPassword";
    private static final String TEST_NAME = "testName";
    private static final String TEST_TOKEN = "testToken";
    private AuthenticationServiceImpl sut;

    private TokenService tokenService;
    private UserDAO userDAO;

    private PasswordVerificationService passwordVerificationService;
    private TokenProperties tokenProperties;

    @BeforeEach
    void setUp() {
        sut = new AuthenticationServiceImpl();
        tokenService = mock(TokenService.class);
        userDAO = mock(UserDAO.class);
        passwordVerificationService = mock(PasswordVerificationService.class);
        tokenProperties = mock(TokenProperties.class);
        sut.setTokenService(tokenService);
        sut.setUserDAO(userDAO);
        sut.setPasswordVerificationService(passwordVerificationService);
        sut.setTokenProperties(tokenProperties);
    }

    @Test
    void loginShouldCallUserDAOGetUser() {
        //arrange
        var loginDto = createLoginDto();
        when(passwordVerificationService.validate(anyString(),anyString())).thenReturn(true);
        when(tokenService.generateToken(any())).thenReturn(createToken());
        when(userDAO.getUserByUsername(anyString()))
                .thenReturn(createUserDto());
        //act
        sut.login(loginDto);

        //assert
        verify(userDAO).getUserByUsername(loginDto.getUser());
    }

    @Test
    void loginShouldCatchUserNotFoundExceptionAndThrowInvalidCredentialsException() {
        var loginDto = createLoginDto();
        when(userDAO.getUserByUsername(anyString())).thenThrow(UserNotFoundException.class);

        assertThrows(InvalidCredentialsException.class, () -> sut.login(loginDto));
    }

    @Test
    void loginShouldThrowInvalidCredentialsException() {
        var loginDto = createLoginDto();
        when(passwordVerificationService.validate(anyString(),anyString())).thenReturn(false);
        when(userDAO.getUserByUsername(anyString()))
                .thenReturn(createUserDto());
        assertThrows(InvalidCredentialsException.class, () -> {
            sut.login(loginDto);
        });
    }

    @Test
    void loginShouldCallTokenServiceGenerateTokenAndAddToExistingTokens() {
        //arrange
        var loginDto = createLoginDto();
        var tokenDto = createToken();
        var userDto = createUserDto();
        when(passwordVerificationService.validate(anyString(),anyString())).thenReturn(true);
        when(userDAO.getUserByUsername(anyString()))
                .thenReturn(userDto);
        when(tokenService.generateToken(userDto))
                .thenReturn(tokenDto);

        //act
        var actual = sut.login(loginDto);

        //assert
        verify(tokenService).generateToken(userDto);
        assertEquals(tokenDto, actual);
    }

    private UserDTO createUserDto() {
        UserDTO userDTO = new UserDTO();
        return userDTO.setUsername(TEST_USER).setPassword(TEST_PASSWORD).setName(TEST_NAME);
    }

    @Test
    void authenticateWithWrongTokenShouldThrowInvalidTokenExceptionWithWrongToken() {
        var token = createToken();
        var wrongToken = token.getToken() + "wrong";
        when(userDAO.getUserByUsername(anyString()))
                .thenReturn(createUserDto());

        assertThrows(InvalidTokenException.class, () -> {
            sut.authenticate(wrongToken);
        });
    }

    @Test
    void authenticateWithRightTokenShouldRunWithoutException() {
        var token = createToken();
        var rightToken = token.getToken();

        when(tokenProperties.getUser(rightToken)).thenReturn(token.getUser());

        assertDoesNotThrow(() -> sut.authenticate(rightToken));
    }

    private LoginDTO createLoginDto() {
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setPassword(TEST_PASSWORD);
        loginDTO.setUser(TEST_USER);
        return loginDTO;
    }

    private TokenDTO createToken() {
        TokenDTO tokenDTO = new TokenDTO();
        tokenDTO.setUser(TEST_USER);
        tokenDTO.setToken(TEST_TOKEN);
        return tokenDTO;
    }
}