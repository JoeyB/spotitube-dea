package nl.mojiweb.joey.han.spotitube.resource;

import nl.mojiweb.joey.han.spotitube.service.authentication.AuthenticationService;
import nl.mojiweb.joey.han.spotitube.dto.LoginDTO;
import nl.mojiweb.joey.han.spotitube.dto.TokenDTO;
import nl.mojiweb.joey.han.spotitube.exception.InvalidCredentialsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;


class LoginResourceTest {

    private LoginResource sut;
    private AuthenticationService authenticationService;

    @BeforeEach
    void setUp() {
        sut = new LoginResource();
        authenticationService = mock(AuthenticationService.class);
        sut.setAuthenticationService(authenticationService);
    }

    @Test
    void loginWithUsernameAndPasswordShouldReturnToken() {
        //arrange
        var myUser = "myUser";
        var loginDto = createLogin(myUser, "mypassword");
        var expectedToken = createToken(myUser);
        when(authenticationService.login(loginDto)).thenReturn(expectedToken);
        //act
        var result = sut.login(loginDto);

        //assert
        assertEquals(expectedToken, result.getEntity());
    }

    @Test
    void loginWithUsernameAndPasswordShouldThrowInvalidCredentialsException() {
        //arrange
        var myUser = "wrongUser";
        var loginDto = createLogin(myUser, "wrongPassword");
        doThrow(new InvalidCredentialsException()).when(authenticationService).login(loginDto);

        //act + assert
        assertThrows(InvalidCredentialsException.class, () -> sut.login(loginDto));
    }

    private TokenDTO createToken(String user) {
        var tokenDto = new TokenDTO();
        tokenDto.setToken("1234-1234-1234");
        tokenDto.setUser(user);
        return tokenDto;
    }

    private LoginDTO createLogin(String user, String password) {
        var loginDto = new LoginDTO();
        loginDto.setPassword(password);
        loginDto.setUser(user);
        return loginDto;
    }

}