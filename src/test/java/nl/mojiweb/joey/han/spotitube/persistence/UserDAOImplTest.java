package nl.mojiweb.joey.han.spotitube.persistence;

import nl.mojiweb.joey.han.spotitube.dto.UserDTO;
import nl.mojiweb.joey.han.spotitube.exception.UserNotFoundException;
import nl.mojiweb.joey.han.spotitube.persistence.dbconnection.ConnectionFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class UserDAOImplTest {

    private UserDAOImpl sut;
    private ConnectionFactory connectionFactory;

    private final static String USER = "testUsername";

    @BeforeEach
    void setUp() {
        connectionFactory = DatabaseProvider.prepareConnectionFactory();
        sut = new UserDAOImpl();
        sut.setConnectionFactory(connectionFactory);
    }

    @Test
    void getUserByUsernameShouldReturnUser() {
        var actual = sut.getUserByUsername(USER);

        assertEquals(actual.getUsername(), USER);
        assertEquals(actual.getPassword(), "testPassword");
        assertEquals(actual.getName(), "testName");
    }

    @Test
    void getUserByUsernameWithWrongUsernameShouldThrowUserNotFoundException() {
        assertThrows(UserNotFoundException.class, () -> sut.getUserByUsername(USER + "wrong"));
    }

    @AfterEach
    void tearDown() throws SQLException {
        StringBuilder myQuery = new StringBuilder("drop all objects delete files");
        connectionFactory.getConnection().createStatement().execute(myQuery.toString());
    }
}