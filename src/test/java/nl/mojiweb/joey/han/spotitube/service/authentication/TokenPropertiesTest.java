package nl.mojiweb.joey.han.spotitube.service.authentication;

import nl.mojiweb.joey.han.spotitube.exception.PropertyFileMissingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class TokenPropertiesTest {

    private TokenProperties sut;


    @BeforeEach
    void setUp() {
        sut = new TokenProperties();
        sut.reset();
    }

    @Test
    void saveTokenShouldAddNewToken() {
        String user = "test_user";
        String token = "test_token";
        sut.saveToken(token, user);
        assertEquals(user, sut.getUser(token));
    }

    @Test
    void saveTokenShouldThrowPropertyFileMissingExceptionWhenFileDoesNotExist() {
        sut = new TokenProperties("fake.file");
        sut.reset();
        String user = "any_user";
        String token = "any_token";
        assertThrows(PropertyFileMissingException.class,() -> sut.saveToken(token, user));
    }

    @Test
    void saveTokenWithInvalidFilePathShouldThrowPropertyFileMissingException() {
        sut = new TokenProperties("/");
        sut.reset();
        String user = "any_user";
        String token = "any_token";
        try{
            sut.getUser(token);
        } catch (PropertyFileMissingException e) {
            assertThrows(PropertyFileMissingException.class,() -> sut.saveToken(token, user));
        }
    }

    @Test
    void getUserShouldReturnNull() {
        String token = "any_token";
        assertNull(sut.getUser(token));
    }
}