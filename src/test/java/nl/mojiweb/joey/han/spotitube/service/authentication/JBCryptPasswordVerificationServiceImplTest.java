package nl.mojiweb.joey.han.spotitube.service.authentication;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JBCryptPasswordVerificationServiceImplTest {

    private JBCryptPasswordVerificationServiceImpl sut;

    @BeforeEach
    void setUp() {
        sut = new JBCryptPasswordVerificationServiceImpl();
    }

    @Test
    void testValidatePasswordShouldValidateWithHashedPassword() {
        String unHashed = "my unhashed pass";
        String hashed = sut.hashPassword(unHashed);
        String wrongHashed = sut.hashPassword(unHashed + ".");
        assertTrue(sut.validate(hashed, unHashed));
        assertFalse(sut.validate(wrongHashed, unHashed));
    }

    @Test
    void testValidatePlainPasswordWithoutHashAvailable() {
        String unHashed = "my unhashed pass";
        String wrong = "wrong pass";
        assertTrue(sut.validate(unHashed,unHashed));
        assertFalse(sut.validate(unHashed, wrong));
    }
}