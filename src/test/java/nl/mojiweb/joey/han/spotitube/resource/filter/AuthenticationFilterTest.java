package nl.mojiweb.joey.han.spotitube.resource.filter;

import nl.mojiweb.joey.han.spotitube.resource.annotation.NoAuthentication;
import nl.mojiweb.joey.han.spotitube.service.authentication.AuthenticationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import static org.mockito.Mockito.*;

class AuthenticationFilterTest {

    private AuthenticationFilter sut;
    private ResourceInfo resourceInfo;
    private AuthenticationService authenticationService;

    @BeforeEach
    void setUp() {
        sut = new AuthenticationFilter();
        resourceInfo = mock(ResourceInfo.class);
        authenticationService = mock(AuthenticationService.class);
        sut.setAuthenticationService(authenticationService);
        sut.setResourceInfo(resourceInfo);

        when(authenticationService.authenticate(anyString())).thenReturn("test");
    }

    @Test
    void filterShouldCallAuthenticationServiceAuthenticateWithoutNoAuthenticationAnnotation() throws NoSuchMethodException {
        var context = mock(ContainerRequestContext.class);
        var method = MockClass.class.getDeclaredMethod("doAuth");
        when(resourceInfo.getResourceMethod()).thenReturn(method);
        var uriInfo = mock(UriInfo.class);
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add("token", "1234-1234-1234");
        when(context.getUriInfo()).thenReturn(uriInfo);
        when(uriInfo.getQueryParameters(anyBoolean())).thenReturn(params);

        sut.filter(context);

        verify(authenticationService).authenticate(anyString());
    }


    @Test
    void filterShouldNotCallAuthenticationServiceAuthenticateWithNoAuthenticationAnnotation() throws NoSuchMethodException {
        var context = mock(ContainerRequestContext.class);
        var method = MockClass.class.getDeclaredMethod("dontAuth");
        when(resourceInfo.getResourceMethod()).thenReturn(method);

        sut.filter(context);

        verify(authenticationService, never()).authenticate(anyString());
    }

    @Test
    void filterShouldCallContainerRequestContextAbortWithWithNoTokenProvided() throws NoSuchMethodException {
        var context = mock(ContainerRequestContext.class);
        var method = MockClass.class.getDeclaredMethod("doAuth");
        when(resourceInfo.getResourceMethod()).thenReturn(method);
        var uriInfo = mock(UriInfo.class);
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        when(context.getUriInfo()).thenReturn(uriInfo);
        when(uriInfo.getQueryParameters(anyBoolean())).thenReturn(params);

        sut.filter(context);

        verify(context).abortWith(any(Response.class));
    }


    private static class MockClass {
        @NoAuthentication
        void dontAuth() {}

        void doAuth() {}
    }

}