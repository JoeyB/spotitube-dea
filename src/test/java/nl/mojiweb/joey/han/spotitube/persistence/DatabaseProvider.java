package nl.mojiweb.joey.han.spotitube.persistence;

import nl.mojiweb.joey.han.spotitube.persistence.dbconnection.ConnectionFactory;
import nl.mojiweb.joey.han.spotitube.persistence.dbconnection.ConnectionFactoryImpl;
import nl.mojiweb.joey.han.spotitube.persistence.dbconnection.DatabaseProperties;
import org.h2.tools.RunScript;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;

class DatabaseProvider {

    static ConnectionFactory prepareConnectionFactory() {
        DatabaseProperties properties = new DatabaseProperties();
        ConnectionFactory connectionFactory = new ConnectionFactoryImpl(properties);
        Connection connection = connectionFactory.getConnection();
        populateDatabase(connection);
        return connectionFactory;
    }

    private static void populateDatabase(Connection connection) {
        executeQuery("src/test/resources/query/create.sql", connection);
        executeQuery("src/test/resources/query/insert.sql", connection);
    }

    private static void executeQuery(String queryPath, Connection connection) {
        try {
            RunScript.execute(connection, new FileReader(queryPath));
        } catch (SQLException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
