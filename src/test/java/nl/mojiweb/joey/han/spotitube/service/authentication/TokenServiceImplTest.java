package nl.mojiweb.joey.han.spotitube.service.authentication;

import nl.mojiweb.joey.han.spotitube.dto.LoginDTO;
import nl.mojiweb.joey.han.spotitube.dto.UserDTO;
import nl.mojiweb.joey.han.spotitube.service.authentication.TokenServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TokenServiceImplTest {

    private TokenServiceImpl sut;

    @BeforeEach
    void setUp() {
        sut = new TokenServiceImpl();
    }

    @Test
    void generateTokenShouldReturnTokenWithUsernameAndValidUUID() {
        var loginDto = createLoginDto();
        var userDto = createUserDto();

        var actual = sut.generateToken(userDto);

        assertEquals(actual.getUser(), loginDto.getUser());
        assertDoesNotThrow(() -> UUID.fromString(actual.getToken()));
    }

    private LoginDTO createLoginDto() {
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUser("testUser");
        loginDTO.setPassword("testPassword");
        return loginDTO;
    }

    private UserDTO createUserDto() {
        return new UserDTO().setName("testPassword").setUsername("testUser").setPassword("testPassword");
    }

}