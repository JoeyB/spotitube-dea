package nl.mojiweb.joey.han.spotitube.resource.exceptionmapper;

import nl.mojiweb.joey.han.spotitube.exception.InvalidCredentialsException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;

class InvalidCredentialsExceptionMapperTest {

    private InvalidCredentialsExceptionMapper sut;

    @BeforeEach
    void setUp() {
        sut = new InvalidCredentialsExceptionMapper();
    }

    @Test
    void toResponseShouldReturnResponseWithStatusHttpUnauthorized() {
        var expected = Response.Status.UNAUTHORIZED.getStatusCode();
        var exception = new InvalidCredentialsException();

        var actual = sut.toResponse(exception);
        Assertions.assertEquals(expected, actual.getStatus());
    }
}