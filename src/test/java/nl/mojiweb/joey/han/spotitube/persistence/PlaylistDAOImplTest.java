package nl.mojiweb.joey.han.spotitube.persistence;

import nl.mojiweb.joey.han.spotitube.dto.PlaylistDTO;
import nl.mojiweb.joey.han.spotitube.exception.ResourceNotFoundException;
import nl.mojiweb.joey.han.spotitube.persistence.dbconnection.ConnectionFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PlaylistDAOImplTest {

    private PlaylistDAOImpl sut;
    private ConnectionFactory connectionFactory;

    private final static String USER = "testUsername";

    @BeforeEach
    void setUp() {
        connectionFactory = DatabaseProvider.prepareConnectionFactory();
        sut = new PlaylistDAOImpl();
        sut.setConnectionFactory(connectionFactory);
    }

    @Test
    void getPlaylistsShouldReturnAllPlaylists() {
        List<PlaylistDTO> expected = new ArrayList<>();
        expected.add(createPlaylist(1, "testPlaylist1"));
        expected.add(createPlaylist(2, "testPlaylist2"));
        List<PlaylistDTO> actual = sut.getPlaylists(USER);
        assertPlaylists(expected, actual);
    }

    @Test
    void getTotalPlaylistDuration() {
        assertEquals(50, sut.getTotalPlaylistDuration());
    }

    @Test
    void addPlaylistShouldAddPlaylistToDatabase() {
        PlaylistDTO playlistDTO = new PlaylistDTO();
        playlistDTO.setName("testPlaylist3");
        List<PlaylistDTO> expected = new ArrayList<>();
        expected.add(createPlaylist(1, "testPlaylist1"));
        expected.add(createPlaylist(2, "testPlaylist2"));
        expected.add(createPlaylist(3, "testPlaylist3"));

        sut.addPlaylist(playlistDTO, USER);

        List<PlaylistDTO> actual = sut.getPlaylists(USER);
        assertPlaylists(expected, actual);
    }

    @Test
    void deletePlaylist() {
        int id = 1;
        List<PlaylistDTO> expected = new ArrayList<>();
        expected.add(createPlaylist(2, "testPlaylist2"));

        sut.deletePlaylist(id);

        List<PlaylistDTO> actual = sut.getPlaylists(USER);
        assertPlaylists(expected, actual);
    }

    @Test
    void updateShouldUpdatePlaylistsInDatabase() {
        PlaylistDTO playlistDTO = new PlaylistDTO();
        playlistDTO.setName("testPlaylist3");
        playlistDTO.setId(2);
        List<PlaylistDTO> expected = new ArrayList<>();
        expected.add(createPlaylist(1, "testPlaylist1"));
        expected.add(createPlaylist(2, "testPlaylist3"));

        sut.update(playlistDTO);

        List<PlaylistDTO> actual = sut.getPlaylists(USER);
        assertPlaylists(expected, actual);
    }

    @Test
    void getPlaylistShouldReturnCorrectPlaylistWithExistingId() {
        int id = 1;
        PlaylistDTO actual = sut.getPlaylist(id);
        assertEquals(id, actual.getId());
        assertEquals("testPlaylist1", actual.getName());
        assertTrue(actual.isOwner());
    }

    @Test
    void getPlaylistShouldThrowResourceNotFoundExceptionWithNonExistingId() {
        int id = 4;
        assertThrows(ResourceNotFoundException.class,() -> sut.getPlaylist(id));
    }

    @AfterEach
    void tearDown() throws SQLException {
        StringBuilder myQuery = new StringBuilder("drop all objects delete files");
        connectionFactory.getConnection().createStatement().execute(myQuery.toString());
    }

    private PlaylistDTO createPlaylist(int id, String name) {
        PlaylistDTO playlistDTO = new PlaylistDTO();
        playlistDTO.setName(name);
        playlistDTO.setId(id);
        playlistDTO.setOwner(true);
        return playlistDTO;
    }

    private void assertPlaylists(List<PlaylistDTO> expected, List<PlaylistDTO> actual) {
        assertEquals(expected.size(), actual.size());
        for(int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i).getId(), actual.get(i).getId());
            assertEquals(expected.get(i).getName(), actual.get(i).getName());
            assertEquals(expected.get(i).isOwner(), actual.get(i).isOwner());
        }
    }
}