package nl.mojiweb.joey.han.spotitube.resource.exceptionmapper;

import nl.mojiweb.joey.han.spotitube.exception.InvalidTokenException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;

class InvalidTokenExceptionMapperTest {

    private InvalidTokenExceptionMapper sut;

    @BeforeEach
    void setUp() {
        sut = new InvalidTokenExceptionMapper();
    }

    @Test
    void toResponseShouldReturnResponseWithStatusHttpNotFound() {
        var expected = Response.Status.FORBIDDEN.getStatusCode();
        var exception = new InvalidTokenException();

        var actual = sut.toResponse(exception);
        Assertions.assertEquals(expected, actual.getStatus());
    }
}