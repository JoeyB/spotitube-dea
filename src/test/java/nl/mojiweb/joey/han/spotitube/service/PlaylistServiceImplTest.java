package nl.mojiweb.joey.han.spotitube.service;

import nl.mojiweb.joey.han.spotitube.dto.PlaylistCollectionDTO;
import nl.mojiweb.joey.han.spotitube.dto.PlaylistDTO;
import nl.mojiweb.joey.han.spotitube.exception.ResourceNotFoundException;
import nl.mojiweb.joey.han.spotitube.persistence.PlaylistDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PlaylistServiceImplTest {

    private static final String USER = "my_user";
    private static final int ID = 1;

    @Mock
    PlaylistDAO playlistDAO;

    @InjectMocks
    private PlaylistServiceImpl sut;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getAllPlaylistsShouldReturnPlaylistCollection() {
        List<PlaylistDTO> playlistDTOS = new ArrayList<>();
        when(playlistDAO.getPlaylists(Mockito.anyString()))
                .thenReturn(playlistDTOS);

        var actual = sut.getAllPlaylists(USER);

        assertEquals(playlistDTOS, actual.getPlaylists());

    }

    @Test
    void getAllPlaylistsShouldCallPlaylistDAO() {
        sut.getAllPlaylists(USER);

        verify(playlistDAO).getPlaylists(USER);
    }

    @Test
    void deletePlaylistShouldCallPlaylistDAO() {
        sut.deletePlaylist(ID);

        verify(playlistDAO).deletePlaylist(ID);
    }

    @Test
    void addPlaylistShouldCallPlaylistDAO() {
        PlaylistDTO playlistDTO = new PlaylistDTO();
        sut.addPlaylist(playlistDTO, USER);

        verify(playlistDAO).addPlaylist(playlistDTO, USER);
    }

    @Test
    void updatePlaylist() {
        PlaylistDTO playlistDTO = new PlaylistDTO();
        sut.updatePlaylist(ID, playlistDTO);

        verify(playlistDAO).update(playlistDTO);
    }

    @Test
    void getPlaylistShouldReturnPlaylist() {
        PlaylistDTO playlistDTO = new PlaylistDTO();
        when(playlistDAO.getPlaylist(ID)).thenReturn(playlistDTO);

        var actual = sut.getPlaylist(ID);

        assertEquals(playlistDTO, actual);
    }

    @Test
    void getPlaylistShouldThrowResourceNotFoundException() {
        when(playlistDAO.getPlaylist(anyInt())).thenThrow(ResourceNotFoundException.class);

        assertThrows(ResourceNotFoundException.class,() -> sut.getPlaylist(ID));
    }
}