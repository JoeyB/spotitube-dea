package nl.mojiweb.joey.han.spotitube.resource.exceptionmapper;

import nl.mojiweb.joey.han.spotitube.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.json.stream.JsonParsingException;
import javax.ws.rs.core.Response;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class ResourceNotFoundExceptionMapperTest {

    private ResourceNotFoundExceptionMapper sut;

    @BeforeEach
    void setUp() {
        sut = new ResourceNotFoundExceptionMapper();
    }

    @Test
    void toResponseShouldReturnStatusNotFound() {
        var expected = Response.Status.NOT_FOUND.getStatusCode();
        var exception = mock(ResourceNotFoundException.class);

        var actual = sut.toResponse(exception);
        Assertions.assertEquals(expected, actual.getStatus());
    }
}