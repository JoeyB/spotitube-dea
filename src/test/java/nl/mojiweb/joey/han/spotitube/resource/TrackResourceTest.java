package nl.mojiweb.joey.han.spotitube.resource;

import nl.mojiweb.joey.han.spotitube.exception.IdFormatException;
import nl.mojiweb.joey.han.spotitube.service.TrackService;
import nl.mojiweb.joey.han.spotitube.dto.TrackCollectionDTO;
import nl.mojiweb.joey.han.spotitube.dto.TrackDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TrackResourceTest {

    //system
    private TrackResource sut;
    //services
    private TrackService trackService;

    @BeforeEach
    void setUp() {
        sut = new TrackResource();
        mockServices();
        sut.setTrackService(trackService);
    }

    private void mockServices() {
        trackService = mock(TrackService.class);
    }

    @Test
    void getTracksShouldReturnListOfTracksWithEmptyIdProvided() {
        //arrange
        var expectedCollection = getTrackCollection();
        when(trackService.getAllTracks()).thenReturn(expectedCollection);
        //act
        var response = sut.getTracks("");
        //assert
        assertEquals(expectedCollection, response.getEntity());
    }

    @Test
    void getTracksShouldReturnListOfTracksWithValidIdProvided() {
        //arrange
        var expectedCollection = getTrackCollection();
        when(trackService.getTracksExcludingPlaylist(Mockito.anyInt())).thenReturn(expectedCollection);
        //act
        var response = sut.getTracks("1");
        //assert
        assertEquals(expectedCollection, response.getEntity());
    }

    @Test
    void getTrackShouldThrowIdFormatExceptionWithInvalidNumber() {
        var invalidNumber = "a12b";
        var expected = new IdFormatException();

        assertThrows(expected.getClass(), () -> {
            sut.getTracks(invalidNumber);
        });
    }

    private TrackCollectionDTO getTrackCollection() {
        TrackCollectionDTO trackCollectionDTO = new TrackCollectionDTO();
        trackCollectionDTO.setTracks(new ArrayList<TrackDTO>());
        return trackCollectionDTO;
    }
}