package nl.mojiweb.joey.han.spotitube.resource.exceptionmapper;

import nl.mojiweb.joey.han.spotitube.exception.InvalidIdException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;

class InvalidIdExceptionMapperTest {
    private InvalidIdExceptionMapper sut;

    @BeforeEach
    void setUp() {
        sut = new InvalidIdExceptionMapper();
    }

    @Test
    void toResponseShouldReturnResponseWithStatusHttpNotFound() {
        var expected = Response.Status.NOT_FOUND.getStatusCode();
        var exception = new InvalidIdException();

        var actual = sut.toResponse(exception);
        Assertions.assertEquals(expected, actual.getStatus());
    }
}