package nl.mojiweb.joey.han.spotitube.persistence;

import nl.mojiweb.joey.han.spotitube.dto.TrackDTO;
import nl.mojiweb.joey.han.spotitube.persistence.dbconnection.ConnectionFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TrackDAOImplTest {

    private TrackDAOImpl sut;
    private ConnectionFactory connectionFactory;

    @BeforeEach
    void setUp() {
        connectionFactory = DatabaseProvider.prepareConnectionFactory();
        sut = new TrackDAOImpl();
        sut.setConnectionFactory(connectionFactory);
    }

    @AfterEach
    void tearDown() throws SQLException {
        StringBuilder myQuery = new StringBuilder("drop all objects delete files");
        connectionFactory.getConnection().createStatement().execute(myQuery.toString());
    }

    @Test
    void getTracksShouldReturnAllTracks() {
        var expected = new ArrayList<TrackDTO>();
        expected.add(createTestTrack(1));
        expected.add(createTestTrack(2));
        expected.add(createTestTrack(3));
        var actual = sut.getTracks();

        assertTrackList(expected, actual);
    }

    @Test
    void getTracksExcludingPlaylistShouldReturnTracksWithoutPlaylist() {
        var expected = new ArrayList<TrackDTO>();
        expected.add(createTestTrack(3));
        var actual = sut.getTracksExcludingPlaylist(1);

        assertTrackList(expected, actual);
    }

    @Test
    void getTracksByPlaylistShouldReturnTracksInPlaylist() {
        var expected = new ArrayList<TrackDTO>();
        expected.add(createTestTrack(1));
        expected.add(createTestTrack(2));

        var actual = sut.getTracksByPlaylist(1);

        assertTrackList(expected, actual);
    }

    @Test
    void addTrackToPlaylistShouldAddTrackToPlaylist() {
        var expected = new ArrayList<TrackDTO>();
        var playlistId = 1;
        expected.add(createTestTrack(1));
        expected.add(createTestTrack(2));
        var track = createTestTrack(3);
        expected.add(track);

        sut.addTrackToPlaylist(playlistId, track);

        assertTrackList(expected, sut.getTracksByPlaylist(playlistId));
    }

    @Test
    void removeTrackFromPlaylistShouldRemoveTrackFromPlaylist() {
        var expected = new ArrayList<TrackDTO>();
        var playlistId = 1;
        expected.add(createTestTrack(1));

        sut.removeTrackFromPlaylist(playlistId, 2);

        assertTrackList(expected, sut.getTracksByPlaylist(playlistId));
    }

    private TrackDTO createTestTrack(int trackNr) {
        TrackDTO trackDTO = new TrackDTO();
        trackDTO.setId(trackNr);
        trackDTO.setTitle("testTitle" + trackNr);
        trackDTO.setAlbum("testAlbum" + trackNr);
        trackDTO.setDescription("testDescription" + trackNr);
        trackDTO.setPerformer("testPerformer" + trackNr);
        trackDTO.setDuration(10);
        trackDTO.setPlaycount(0);
        trackDTO.setPublicationDate("0" + trackNr + "-01-1970");
        return trackDTO;
    }

    private void assertTrackList(List<TrackDTO> expected, List<TrackDTO> actual) {
        assertEquals(expected.size(), actual.size());
        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i).getId(), actual.get(i).getId());
            assertEquals(expected.get(i).getPerformer(), actual.get(i).getPerformer());
            assertEquals(expected.get(i).getAlbum(), actual.get(i).getAlbum());
            assertEquals(expected.get(i).getDuration(), actual.get(i).getDuration());
            assertEquals(expected.get(i).getTitle(), actual.get(i).getTitle());
            assertEquals(expected.get(i).getPlaycount(), actual.get(i).getPlaycount());
            assertEquals(expected.get(i).getPublicationDate(), actual.get(i).getPublicationDate());
            assertEquals(expected.get(i).getDescription(), actual.get(i).getDescription());
        }
    }
}