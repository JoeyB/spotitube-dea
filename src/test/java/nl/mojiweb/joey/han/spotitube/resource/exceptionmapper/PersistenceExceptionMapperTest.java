package nl.mojiweb.joey.han.spotitube.resource.exceptionmapper;

import nl.mojiweb.joey.han.spotitube.exception.InvalidCredentialsException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response;

import static org.junit.jupiter.api.Assertions.*;

class PersistenceExceptionMapperTest {
    private PersistenceExceptionMapper sut;

    @BeforeEach
    void setUp() {
        sut = new PersistenceExceptionMapper();
    }

    @Test
    void toResponseShouldReturnResponseWithStatusHttpUnauthorized() {
        var expected = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
        var exception = new PersistenceException();

        var actual = sut.toResponse(exception);
        Assertions.assertEquals(expected, actual.getStatus());
    }
}