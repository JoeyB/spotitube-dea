package nl.mojiweb.joey.han.spotitube.resource.exceptionmapper;

import nl.mojiweb.joey.han.spotitube.exception.InvalidTokenException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.json.stream.JsonParsingException;
import javax.ws.rs.core.Response;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

class JsonParsingExceptionMapperTest {
    private JsonParsingExceptionMapper sut;

    @BeforeEach
    void setUp() {
        sut = new JsonParsingExceptionMapper();
    }

    @Test
    void toResponseShouldReturnResponseWithStatusHttpBadRequest() {
        var expected = Response.Status.BAD_REQUEST.getStatusCode();
        var exception = mock(JsonParsingException.class);

        var actual = sut.toResponse(exception);
        Assertions.assertEquals(expected, actual.getStatus());
    }
}