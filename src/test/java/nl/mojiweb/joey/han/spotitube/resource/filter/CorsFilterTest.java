package nl.mojiweb.joey.han.spotitube.resource.filter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CorsFilterTest {

    @InjectMocks
    private CorsFilter sut;
    @Mock
    private ContainerResponseContext containerResponseContext;
    @Mock
    private ContainerRequestContext containerRequestContext;

    @BeforeEach
    void setUp() {
        sut = new CorsFilter();
        containerResponseContext = mock(ContainerResponseContext.class);
        containerRequestContext = mock(ContainerRequestContext.class);
    }

    @Test
    void filterShouldAddHeadersContainerRequestContext() throws IOException {
        MultivaluedMap<String, Object> map = new MultivaluedHashMap<>();
        when(containerResponseContext.getHeaders()).thenReturn(map);
        sut.filter(containerRequestContext, containerResponseContext);

        verify(containerResponseContext, times(4)).getHeaders();
        assertEquals(map.getFirst("Access-Control-Allow-Origin"), "*");
        assertEquals(map.getFirst("Access-Control-Allow-Credentials"), "true");
        assertEquals(map.getFirst("Access-Control-Allow-Headers"), "origin, content-type, accept, authorization");
        assertEquals(map.getFirst("Access-Control-Allow-Methods"), "GET, POST, PUT, DELETE, OPTIONS, HEAD");
    }
}