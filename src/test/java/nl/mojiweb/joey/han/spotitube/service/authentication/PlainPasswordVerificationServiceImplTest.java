package nl.mojiweb.joey.han.spotitube.service.authentication;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlainPasswordVerificationServiceImplTest {

    private PlainPasswordVerificationServiceImpl sut;

    @BeforeEach
    void setUp() {
        sut = new PlainPasswordVerificationServiceImpl();
    }

    @Test
    void validateShouldComparePasswords() {
        String right = "goodpass";
        String wrongPass = "wrongPass";
        assertFalse(sut.validate(right, wrongPass));
        assertTrue(sut.validate(right, right));
    }

    @Test
    void hashPasswordShouldReturnPlainPassword() {
        String expected = "myPass";
        String actual = sut.hashPassword(expected);
        assertEquals(expected, actual);
    }
}