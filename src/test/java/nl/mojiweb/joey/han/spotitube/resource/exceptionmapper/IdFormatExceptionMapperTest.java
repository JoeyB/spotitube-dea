package nl.mojiweb.joey.han.spotitube.resource.exceptionmapper;

import nl.mojiweb.joey.han.spotitube.exception.IdFormatException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;

class IdFormatExceptionMapperTest {

    private IdFormatExceptionMapper sut;

    @BeforeEach
    void setUp() {
        sut = new IdFormatExceptionMapper();
    }

    @Test
    void toResponseShouldReturnResponseWithStatusHttpBadRequest() {
        var expected = Response.Status.BAD_REQUEST.getStatusCode();
        var exception = new IdFormatException();

        var actual = sut.toResponse(exception);
        Assertions.assertEquals(expected, actual.getStatus());
    }
}