package nl.mojiweb.joey.han.spotitube.resource;

import nl.mojiweb.joey.han.spotitube.service.PlaylistService;
import nl.mojiweb.joey.han.spotitube.dto.PlaylistCollectionDTO;
import nl.mojiweb.joey.han.spotitube.dto.PlaylistDTO;
import nl.mojiweb.joey.han.spotitube.dto.TrackCollectionDTO;
import nl.mojiweb.joey.han.spotitube.dto.TrackDTO;
import nl.mojiweb.joey.han.spotitube.service.TrackService;
import nl.mojiweb.joey.han.spotitube.exception.InvalidIdException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.security.Principal;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PlaylistResourceTest {

    private static final int HTTP_OK = Response.Status.OK.getStatusCode();
    private static final int HTTP_CREATED = Response.Status.CREATED.getStatusCode();

    private PlaylistResource sut;
    @Mock
    private PlaylistService playlistService;
    @Mock
    private TrackService trackService;
    @Mock
    private SecurityContext securityContext;
    @Mock
    private Principal securityPrincipal;

    @BeforeEach
    void setUp() {
        sut = new PlaylistResource();
        playlistService = mock(PlaylistService.class);
        sut.setPlaylistService(playlistService);
        sut.setSecurityContext(securityContext);
        sut.setTrackService(trackService);
    }

    @Test
    void getPlaylistCollectionShouldReturnPlayListCollection() {
        var expected = createPlaylistCollectionDto();
        when(playlistService.getAllPlaylists(anyString())).thenReturn(expected);
        when(securityContext.getUserPrincipal()).thenReturn(securityPrincipal);
        when(securityPrincipal.getName()).thenReturn("");

        var actual = sut.getPlaylistCollection();

        assertEquals(expected, actual.getEntity());
    }

    @Test
    void getPlaylistCollectionShouldReturnResponseWithStatusOk() {
        var expected = createPlaylistCollectionDto();
        when(playlistService.getAllPlaylists(anyString())).thenReturn(expected);
        when(securityContext.getUserPrincipal()).thenReturn(securityPrincipal);
        when(securityPrincipal.getName()).thenReturn("");
        var id = 1;

        var actual = sut.deletePlaylist(id);

        assertEquals(HTTP_OK, actual.getStatus());
    }

    @Test
    void deletePlaylistShouldReturnResponseWithStatusOk() {
        var expected = createPlaylistCollectionDto();
        when(playlistService.getAllPlaylists(anyString())).thenReturn(expected);
        when(securityContext.getUserPrincipal()).thenReturn(securityPrincipal);
        when(securityPrincipal.getName()).thenReturn("");
        var id = 1;

        var actual = sut.deletePlaylist(id);

        assertEquals(HTTP_OK, actual.getStatus());
    }

    @Test
    void deletePlaylistShouldCallPlaylistService() {
        var expected = createPlaylistCollectionDto();
        when(playlistService.getAllPlaylists(anyString())).thenReturn(expected);
        when(securityContext.getUserPrincipal()).thenReturn(securityPrincipal);
        when(securityPrincipal.getName()).thenReturn("");
        var id = 1;

        sut.deletePlaylist(id);

        verify(playlistService).deletePlaylist(Mockito.anyInt());
    }

    @Test
    void deletePlaylistShouldThrowInvalidIdException() {
        var invalidId = -123;
        doThrow(new InvalidIdException()).when(playlistService).deletePlaylist(Mockito.anyInt());

        assertThrows(InvalidIdException.class, () -> {
            sut.deletePlaylist(invalidId);
        });
    }

    @Test
    void addPlaylistShouldReturnAllPlaylists() {
        var expected = createPlaylistCollectionDto();
        var playlist = createPlaylistDto();
        when(playlistService.getAllPlaylists(anyString())).thenReturn(expected);
        when(securityContext.getUserPrincipal()).thenReturn(securityPrincipal);
        when(securityPrincipal.getName()).thenReturn("");

        var actual = sut.addPlaylist(playlist);

        assertEquals(expected, actual.getEntity());
    }

    private PlaylistCollectionDTO createPlaylistCollectionDto() {
        PlaylistCollectionDTO playlistCollectionDTO = new PlaylistCollectionDTO();
        playlistCollectionDTO.setLength(0);
        playlistCollectionDTO.setPlaylists(new ArrayList<PlaylistDTO>());
        return playlistCollectionDTO;
    }

    @Test
    void addPlaylistShouldCallPlayListServiceAddPlaylist() {
        var expected = createPlaylistCollectionDto();
        var playlist = createPlaylistDto();
        when(playlistService.getAllPlaylists(anyString())).thenReturn(expected);
        when(securityContext.getUserPrincipal()).thenReturn(securityPrincipal);
        when(securityPrincipal.getName()).thenReturn("");

        sut.addPlaylist(playlist);

        verify(playlistService).addPlaylist(playlist, "");
    }

    @Test
    void addPlayListShouldReturnResponseWithStatusCreated() {
        var expected = createPlaylistCollectionDto();
        when(playlistService.getAllPlaylists(anyString())).thenReturn(expected);
        when(securityContext.getUserPrincipal()).thenReturn(securityPrincipal);
        when(securityPrincipal.getName()).thenReturn("");
        var playlist = createPlaylistDto();

        var actual = sut.addPlaylist(playlist);

        assertEquals(HTTP_CREATED, actual.getStatus());
    }

    private PlaylistDTO createPlaylistDto() {
        PlaylistDTO playlistDTO = new PlaylistDTO();
        playlistDTO.setId(-1);
        playlistDTO.setName("testName");
        playlistDTO.setTracks(new ArrayList<>());
        playlistDTO.setOwner(false);
        return playlistDTO;
    }

    @Test
    void editPlaylistShouldReturnResponseWithStatusOk() {
        when(securityContext.getUserPrincipal()).thenReturn(securityPrincipal);
        when(securityPrincipal.getName()).thenReturn("");
        var playlist = createPlaylistDto();
        var id = 1;

        var actual = sut.editPlaylist(id, playlist);

        assertEquals(HTTP_OK, actual.getStatus());
    }

    @Test
    void editPlaylistShouldReturnAllPlaylists() {
        var id = 1;
        var playlist = createPlaylistDto();
        var expected = createPlaylistCollectionDto();
        when(playlistService.getAllPlaylists(anyString())).thenReturn(expected);
        when(securityContext.getUserPrincipal()).thenReturn(securityPrincipal);
        when(securityPrincipal.getName()).thenReturn("");

        var actual = sut.editPlaylist(id, playlist);

        assertEquals(expected, actual.getEntity());
    }

    @Test
    void editPlaylistShouldThrowInvalidIdException() {
        var invalidId = -123;
        var playlist = createPlaylistDto();
        doThrow(new InvalidIdException()).when(playlistService).updatePlaylist(invalidId, playlist);

        assertThrows(InvalidIdException.class, () -> {
            sut.editPlaylist(invalidId, playlist);
        });
    }

    @Test
    void getTracksForPlaylistShouldReturnListOfTracks() {
        var trackList = getTrackCollection();
        var id = 1;
        when(trackService.getTracksByPlaylist(anyInt())).thenReturn(trackList);

        var actual = sut.getTracksForPlaylist(id);

        assertEquals(trackList, actual.getEntity());
    }

    @Test
    void getTracksForPlaylistShouldReturnResponseWithStatusOk() {
        int id = 1;

        var actual = sut.getTracksForPlaylist(id);

        assertEquals(HTTP_OK, actual.getStatus());
    }

    @Test
    void getTracksForPlaylistShouldThrowInvalidIdException() {
        var invalidId = -123;
        doThrow(new InvalidIdException()).when(trackService).getTracksByPlaylist(invalidId);

        assertThrows(InvalidIdException.class, () -> {
            sut.getTracksForPlaylist(invalidId);
        });
    }

    @Test
    void deleteTrackForPlaylistShouldCallPlaylistServiceDeleteTrackForPlaylist() {
        var listId = 1;
        var trackId = 2;

        sut.deleteTrackForPlaylist(listId, trackId);

        verify(trackService).removeTrackFromPlaylist(listId, trackId);
    }

    @Test
    void deleteTrackForPlaylistShouldReturnResponseWithStatusOk() {
        var listId = 1;
        var trackId = 2;

        var actual = sut.deleteTrackForPlaylist(listId, trackId);

        assertEquals(HTTP_OK, actual.getStatus());
    }

    @Test
    void deleteTrackForPlaylistShouldReturnAllTracksForPlaylists() {
        var listId = 1;
        var trackId = 2;
        var expected = getTrackCollection();
        when(trackService.getTracksByPlaylist(listId)).thenReturn(expected);

        var actual = sut.deleteTrackForPlaylist(listId, trackId);

        assertEquals(expected, actual.getEntity());
    }

    @Test
    void deleteTrackForPlaylistShouldThrowInvalidIdException() {
        var invalidListId = -123;
        var trackId = -123;
        doThrow(new InvalidIdException()).when(trackService).removeTrackFromPlaylist(invalidListId, trackId);

        assertThrows(InvalidIdException.class, () -> {
            sut.deleteTrackForPlaylist(invalidListId, trackId);
        });
    }

    @Test
    void addTrackForPlaylistShouldCallPlayListServiceAddTrackForPlaylist() {
        var id = 1;
        var trackDto = createTrackDto();

        sut.addTrackForPlaylist(id, trackDto);

        verify(trackService).addTrackForPlaylist(id,trackDto);
    }

    private TrackDTO createTrackDto() {
        TrackDTO trackDTO = new TrackDTO();
        trackDTO.setAlbum("testAlbum");
        trackDTO.setDescription("testDescription");
        trackDTO.setPublicationDate("10-05-2019");
        trackDTO.setDuration(300);
        trackDTO.setPerformer("testPerformer");
        trackDTO.setPlaycount(-1);
        trackDTO.setTitle("testTrack");
        trackDTO.setId(1);
        trackDTO.setOfflineAvailable(false);
        return trackDTO;
    }

    @Test
    void addTrackForPlaylistShouldReturnResponseWithStatusCreated() {
        var playlist = createTrackDto();
        var id = 1;

        var actual = sut.addTrackForPlaylist(id, playlist);

        assertEquals(HTTP_CREATED, actual.getStatus());
    }

    @Test
    void addTrackForPlaylistShouldReturnAllTracksForPlaylist() {
        var expected = getTrackCollection();
        var id = 1;
        var track = createTrackDto();
        when(trackService.getTracksByPlaylist(id)).thenReturn(expected);

        var actual = sut.addTrackForPlaylist(id, track);

        assertEquals(expected, actual.getEntity());
    }

    @Test
    void addTrackForPlaylistShouldThrowInvalidIdException() {
        var invalidListId = -123;
        var track = createTrackDto();
        doThrow(new InvalidIdException()).when(trackService).addTrackForPlaylist(invalidListId, track);

        assertThrows(InvalidIdException.class, () -> {
            sut.addTrackForPlaylist(invalidListId, track);
        });
    }

    private TrackCollectionDTO getTrackCollection() {
        TrackCollectionDTO trackCollectionDTO = new TrackCollectionDTO();
        trackCollectionDTO.setTracks(new ArrayList<TrackDTO>());
        return trackCollectionDTO;
    }
}