package nl.mojiweb.joey.han.spotitube.resource.security;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SpotitubeSecurityContextTest {

    private SpotitubeSecurityContext sut;
    private static final String NAME = "anyName";

    @BeforeEach
    void setUp() {
        sut = new SpotitubeSecurityContext(NAME);
    }

    @Test
    void getUserPrincipalShouldReturnUserPrincipleWithName() {
        assertEquals(NAME, sut.getUserPrincipal().getName());
    }

    @Test
    void isUserInRoleShouldReturnFalse() {
        assertFalse(sut.isUserInRole("any string"));
    }

    @Test
    void isSecureShouldReturnFalse() {
        assertFalse(sut.isSecure());
    }

    @Test
    void getAuthenticationSchemeShouldReturnNull() {
        assertNull(sut.getAuthenticationScheme());
    }
}