package nl.mojiweb.joey.han.spotitube.persistence;

import nl.mojiweb.joey.han.spotitube.dto.TrackDTO;

import java.util.List;

public interface TrackDAO {

    List<TrackDTO> getTracks();

    List<TrackDTO> getTracksExcludingPlaylist(int playlistId);

    List<TrackDTO> getTracksByPlaylist(int playlistId);

    void removeTrackFromPlaylist(int playlistId, int trackId);

    void addTrackToPlaylist(int playlistId, TrackDTO id);
}
