package nl.mojiweb.joey.han.spotitube.service.authentication;

import nl.mojiweb.joey.han.spotitube.dto.LoginDTO;
import nl.mojiweb.joey.han.spotitube.dto.TokenDTO;

public interface AuthenticationService {
    TokenDTO login(LoginDTO loginDTO);

    String authenticate(String token);
}
