package nl.mojiweb.joey.han.spotitube.dto;

public class UserDTO {
    private String username;
    private String password;
    private String name;

    public UserDTO() {
    }

    public String getUsername() {
        return username;
    }

    public UserDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserDTO setPassword(String password) {
        this.password = password;
        return this;
    }

    public UserDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getName() {
        return this.name;
    }
}
