package nl.mojiweb.joey.han.spotitube.service.authentication;

import nl.mojiweb.joey.han.spotitube.dto.TokenDTO;
import nl.mojiweb.joey.han.spotitube.dto.UserDTO;

import java.util.UUID;

public class TokenServiceImpl implements TokenService {
    @Override
    public TokenDTO generateToken(UserDTO userDTO) {
        TokenDTO tokenDTO = new TokenDTO();
        tokenDTO.setUser(userDTO.getUsername());
        tokenDTO.setToken(this.generateTokenString());
        return tokenDTO;
    }

    private String generateTokenString() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
