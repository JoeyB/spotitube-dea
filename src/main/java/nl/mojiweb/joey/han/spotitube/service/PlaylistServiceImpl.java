package nl.mojiweb.joey.han.spotitube.service;

import nl.mojiweb.joey.han.spotitube.persistence.PlaylistDAO;
import nl.mojiweb.joey.han.spotitube.persistence.TrackDAO;
import nl.mojiweb.joey.han.spotitube.dto.PlaylistCollectionDTO;
import nl.mojiweb.joey.han.spotitube.dto.PlaylistDTO;
import nl.mojiweb.joey.han.spotitube.dto.TrackCollectionDTO;
import nl.mojiweb.joey.han.spotitube.dto.TrackDTO;

import javax.enterprise.inject.Default;
import javax.inject.Inject;
import java.util.List;

@Default
public class PlaylistServiceImpl implements PlaylistService {

    private PlaylistDAO playlistDAO;

    @Override
    public PlaylistCollectionDTO getAllPlaylists(String user) {
        List<PlaylistDTO> playlistDTOS = playlistDAO.getPlaylists(user);
        PlaylistCollectionDTO playlistCollectionDTO = new PlaylistCollectionDTO();
        playlistCollectionDTO.setPlaylists(playlistDTOS);
        playlistCollectionDTO.setLength(playlistDAO.getTotalPlaylistDuration());
        return playlistCollectionDTO;
    }

    @Override
    public void deletePlaylist(int id) {
        playlistDAO.deletePlaylist(id);
    }

    @Override
    public void addPlaylist(PlaylistDTO playlistDTO, String user) {
        playlistDAO.addPlaylist(playlistDTO, user);
    }

    @Override
    public void updatePlaylist(int id, PlaylistDTO playlistDTO) {
        playlistDAO.update(playlistDTO);
    }

    @Override
    public PlaylistDTO getPlaylist(int playlistId) {
        return playlistDAO.getPlaylist(playlistId);
    }

    @Inject
    public void setPlaylistDAO(PlaylistDAO playlistDAO) {
        this.playlistDAO = playlistDAO;
    }
}
