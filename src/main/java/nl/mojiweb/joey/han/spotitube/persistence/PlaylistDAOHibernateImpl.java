package nl.mojiweb.joey.han.spotitube.persistence;

import nl.mojiweb.joey.han.spotitube.dto.PlaylistDTO;
import nl.mojiweb.joey.han.spotitube.exception.InvalidCredentialsException;
import nl.mojiweb.joey.han.spotitube.exception.ResourceNotFoundException;
import nl.mojiweb.joey.han.spotitube.persistence.hibernate.HibernateConnectionFactory;
import nl.mojiweb.joey.han.spotitube.persistence.hibernate.PlaylistEntity;
import nl.mojiweb.joey.han.spotitube.persistence.hibernate.UserEntity;
import org.hibernate.HibernateException;

import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import javax.persistence.PersistenceException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Alternative
public class PlaylistDAOHibernateImpl implements PlaylistDAO {

    private HibernateConnectionFactory connectionFactory;

    @Override
    public List<PlaylistDTO> getPlaylists(String user) {
        try (var session = connectionFactory.getSession()) {
            var playlistEntities = session.createQuery("from PlaylistEntity", PlaylistEntity.class).list();
            return playlistEntities.stream().map(e -> {
                PlaylistDTO playlistDTO = new PlaylistDTO();
                playlistDTO.setOwner(e.getUser().getUsername().equals(user));
                playlistDTO.setName(e.getName());
                playlistDTO.setId(e.getId());
                return playlistDTO;
            }).collect(Collectors.toList());
        } catch (HibernateException e) {
            throw new PersistenceException(e);
        }
    }

    @Override
    public int getTotalPlaylistDuration() {
        var stream = IntStream.builder();
        try (var session = connectionFactory.getSession()) {
            var playlistEntities = session.createQuery("from PlaylistEntity", PlaylistEntity.class).list();
            playlistEntities.forEach(p -> p.getTracks().forEach(t -> stream.add(t.getDuration())));
        } catch (HibernateException e) {
            throw new PersistenceException(e);
        }
        return stream.build().sum();
    }

    @Override
    public void addPlaylist(PlaylistDTO playlistDTO, String user) {
        try (var session = connectionFactory.getSession()) {
            var userEntity = session.find(UserEntity.class, user);
            if (userEntity == null) throw new InvalidCredentialsException();
            PlaylistEntity playlistEntity = new PlaylistEntity();
            playlistEntity.setName(playlistDTO.getName());
            playlistEntity.setUser(userEntity);
            var transaction = session.beginTransaction();
            session.persist(playlistEntity);
            transaction.commit();
        } catch (HibernateException e) {
            throw new PersistenceException(e);
        }
    }

    @Override
    public void deletePlaylist(int id) {
        try (var session = connectionFactory.getSession()) {
            var playlistEntity = session.find(PlaylistEntity.class, id);
            if (playlistEntity == null) throw new ResourceNotFoundException();
            var transaction = session.beginTransaction();
            session.delete(playlistEntity);
            transaction.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            throw new PersistenceException(e);
        }
    }

    @Override
    public void update(PlaylistDTO playlistDTO) {
        try(var session = connectionFactory.getSession()) {
            var playlistEntity = session.find(PlaylistEntity.class, playlistDTO.getId());
            if (playlistEntity == null) throw new ResourceNotFoundException();
            playlistEntity.setName(playlistDTO.getName());
            var transaction = session.beginTransaction();
            session.update(playlistEntity);
            transaction.commit();
        }
    }

    @Override
    public PlaylistDTO getPlaylist(int playlistId) {
        try(var session = connectionFactory.getSession()) {
            var entity = session.find(PlaylistEntity.class, playlistId);
            if (entity == null) throw new ResourceNotFoundException();
            PlaylistDTO playlistDTO = new PlaylistDTO();
            playlistDTO.setId(entity.getId());
            playlistDTO.setName(entity.getName());
            playlistDTO.setOwner(true);
            return playlistDTO;
        } catch (HibernateException e) {
            throw new PersistenceException(e);
        }
    }

    @Inject
    public void setConnectionFactory(HibernateConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }
}
