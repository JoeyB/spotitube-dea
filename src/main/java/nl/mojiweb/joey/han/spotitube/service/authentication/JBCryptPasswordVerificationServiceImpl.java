package nl.mojiweb.joey.han.spotitube.service.authentication;

import org.mindrot.BCrypt;

import javax.enterprise.inject.Alternative;

@Alternative
public class JBCryptPasswordVerificationServiceImpl implements PasswordVerificationService {

    private static final int LOG_ROUNDS = 12;

    @Override
    public boolean validate(String password, String passwordToValidate) {
        try {
            return BCrypt.checkpw(passwordToValidate, password);
        } catch (IllegalArgumentException e) {
            return password.equals(passwordToValidate);
        }
    }

    @Override
    public String hashPassword(String passwordToHash) {
        return BCrypt.hashpw(passwordToHash, BCrypt.gensalt(LOG_ROUNDS));
    }
}
