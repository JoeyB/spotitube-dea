package nl.mojiweb.joey.han.spotitube.resource.security;

import javax.ws.rs.core.SecurityContext;
import java.security.Principal;

public class SpotitubeSecurityContext implements SecurityContext {

    private Principal userPrincipal;

    public SpotitubeSecurityContext(String name) {
        userPrincipal = new SpotitubeUserPrincipal(name);
    }

    @Override
    public Principal getUserPrincipal() {
        return userPrincipal;
    }

    @Override
    public boolean isUserInRole(String role) {
        return false;
    }

    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public String getAuthenticationScheme() {
        return null;
    }
}
