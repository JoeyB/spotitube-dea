package nl.mojiweb.joey.han.spotitube.persistence.dbconnection;

import org.apache.commons.dbcp.BasicDataSource;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.PersistenceException;
import java.sql.Connection;
import java.sql.SQLException;

@Singleton
public class ConnectionFactoryImpl implements ConnectionFactory {

    private BasicDataSource ds = new BasicDataSource();

    @Inject
    public ConnectionFactoryImpl(DatabaseProperties properties) {
        try {
            Class.forName(properties.getDriver());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        ds.setUrl(properties.getConnectionString());
        ds.setUsername(properties.getUsername());
        ds.setPassword(properties.getPassword());
        ds.setMinIdle(5);
        ds.setMaxIdle(10);
        ds.setMaxOpenPreparedStatements(100);
    }

    @Override
    public Connection getConnection() throws PersistenceException {
        try {
            return ds.getConnection();
        } catch (SQLException e) {
            throw new PersistenceException(e);
        }
    }

}
