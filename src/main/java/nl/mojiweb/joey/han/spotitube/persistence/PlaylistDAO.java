package nl.mojiweb.joey.han.spotitube.persistence;

import nl.mojiweb.joey.han.spotitube.dto.PlaylistDTO;

import java.util.List;

public interface PlaylistDAO {

    List<PlaylistDTO> getPlaylists(String user);

    int getTotalPlaylistDuration();

    void addPlaylist(PlaylistDTO playlistDTO, String user);

    void deletePlaylist(int id);

    void update(PlaylistDTO playlistDTO);

    PlaylistDTO getPlaylist(int playlistId);
}
