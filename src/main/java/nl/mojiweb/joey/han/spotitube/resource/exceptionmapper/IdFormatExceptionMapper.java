package nl.mojiweb.joey.han.spotitube.resource.exceptionmapper;

import nl.mojiweb.joey.han.spotitube.exception.IdFormatException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class IdFormatExceptionMapper implements ExceptionMapper<IdFormatException> {
    @Override
    public Response toResponse(IdFormatException e) {
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
