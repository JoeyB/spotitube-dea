package nl.mojiweb.joey.han.spotitube.resource.security;

import java.security.Principal;

public class SpotitubeUserPrincipal implements Principal {

    private String name;

    public SpotitubeUserPrincipal(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
