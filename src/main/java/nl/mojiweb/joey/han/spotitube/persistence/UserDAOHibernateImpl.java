package nl.mojiweb.joey.han.spotitube.persistence;

import nl.mojiweb.joey.han.spotitube.dto.UserDTO;
import nl.mojiweb.joey.han.spotitube.exception.InvalidCredentialsException;
import nl.mojiweb.joey.han.spotitube.persistence.hibernate.HibernateConnectionFactory;
import nl.mojiweb.joey.han.spotitube.persistence.hibernate.UserEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import javax.persistence.PersistenceException;

@Alternative
public class UserDAOHibernateImpl implements UserDAO {

    private HibernateConnectionFactory hibernateConnectionFactory;

    @Override
    public UserDTO getUserByUsername(String user) {
        try (Session session = hibernateConnectionFactory.getSession()) {
            UserEntity entity = session.find(UserEntity.class, user);
            if (entity == null) {
                throw new InvalidCredentialsException();
            }
            UserDTO userDTO = new UserDTO();
            userDTO.setName(entity.getName());
            userDTO.setPassword(entity.getPassword());
            userDTO.setUsername(entity.getUsername());
            return userDTO;
        } catch (HibernateException e) {
            e.printStackTrace();
            throw new PersistenceException(e);
        }
    }

    @Inject
    public void setHibernateConnectionFactory(HibernateConnectionFactory hibernateConnectionFactory) {
        this.hibernateConnectionFactory = hibernateConnectionFactory;
    }
}
