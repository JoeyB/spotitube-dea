package nl.mojiweb.joey.han.spotitube.dto;

import java.util.List;

public class TrackCollectionDTO {
    private List<TrackDTO> tracks;

    public void setTracks(List<TrackDTO> tracks) {
        this.tracks = tracks;
    }

    public List<TrackDTO> getTracks() {
        return tracks;
    }
}
