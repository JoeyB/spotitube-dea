package nl.mojiweb.joey.han.spotitube.resource.filter;

import nl.mojiweb.joey.han.spotitube.resource.annotation.NoAuthentication;
import nl.mojiweb.joey.han.spotitube.resource.security.SpotitubeSecurityContext;
import nl.mojiweb.joey.han.spotitube.service.authentication.AuthenticationService;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.util.Arrays;

@Provider
public class AuthenticationFilter implements ContainerRequestFilter {

    private ResourceInfo resourceInfo;

    private AuthenticationService authenticationService;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) {
        if (requiresAuthentication()) {
            String token = getToken(containerRequestContext);
            if (token == null) {
                containerRequestContext.abortWith(Response
                        .status(Response.Status.BAD_REQUEST)
                        .build());
                return;
            }
            containerRequestContext.setSecurityContext(
                    new SpotitubeSecurityContext(
                            authenticationService
                                    .authenticate(token)
                    )
            );
        }

    }

    private boolean requiresAuthentication() {
        return Arrays.stream(resourceInfo.getResourceMethod().getDeclaredAnnotations())
                .noneMatch((annotation) ->
                        annotation.annotationType().equals(NoAuthentication.class)
                );
    }

    private String getToken(ContainerRequestContext containerRequestContext) {
        MultivaluedMap<String, String> queryParams = containerRequestContext.getUriInfo().getQueryParameters(true);
        return queryParams.getFirst("token");
    }

    @Context
    public void setResourceInfo(ResourceInfo resourceInfo) {
        this.resourceInfo = resourceInfo;
    }

    @Inject
    public void setAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }
}
