package nl.mojiweb.joey.han.spotitube.persistence;

import nl.mojiweb.joey.han.spotitube.dto.TrackDTO;
import nl.mojiweb.joey.han.spotitube.persistence.dbconnection.ConnectionFactory;
import nl.mojiweb.joey.han.spotitube.exception.ResourceNotFoundException;

import javax.inject.Inject;
import javax.persistence.PersistenceException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TrackDAOImpl implements TrackDAO {

    private ConnectionFactory connectionFactory;

    @Override
    public List<TrackDTO> getTracks() {
        String query = "SELECT * FROM track";
        List<TrackDTO> trackDTOS = new ArrayList<>();
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                trackDTOS.add(getTrackFromResultSet(resultSet, false));
            }
        } catch (SQLException e) {
            throw new PersistenceException(e);
        }
        return trackDTOS;
    }

    private TrackDTO getTrackFromResultSet(ResultSet resultSet, boolean forPlaylist) throws SQLException {
        TrackDTO trackDTO = new TrackDTO();
        trackDTO.setId(resultSet.getInt("id"));
        trackDTO.setAlbum(resultSet.getString("album"));
        trackDTO.setTitle(resultSet.getString("title"));
        trackDTO.setDescription(resultSet.getString("description"));
        trackDTO.setPublicationDate(resultSet.getString("date"));
        trackDTO.setDuration(resultSet.getInt("duration"));
        trackDTO.setPerformer(resultSet.getString("performer"));
        if (forPlaylist) {
            trackDTO.setOfflineAvailable(resultSet.getBoolean("offlineAvailable"));
        }
        trackDTO.setPlaycount(resultSet.getInt("playcount"));
        return trackDTO;
    }

    @Override
    public List<TrackDTO> getTracksExcludingPlaylist(int playlistId) {
        String query = "SELECT * FROM track WHERE track.id NOT IN (SELECT track_id FROM playlist_track WHERE playlist_id = ?)";
        List<TrackDTO> trackDTOS = new ArrayList<>();
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, playlistId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                trackDTOS.add(getTrackFromResultSet(resultSet, false));
            }
        } catch (SQLException e) {
            throw new PersistenceException(e);
        }
        return trackDTOS;
    }

    @Override
    public List<TrackDTO> getTracksByPlaylist(int id) {
        String query = "SELECT track.*, pt.offlineavailable FROM track INNER JOIN playlist_track pt on track.id = pt.track_id WHERE pt.playlist_id = ?";
        List<TrackDTO> trackDTOS = new ArrayList<>();
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                trackDTOS.add(getTrackFromResultSet(resultSet, true));
            }
        } catch (SQLException e) {
            throw new PersistenceException(e);
        }
        return trackDTOS;
    }

    @Override
    public void addTrackToPlaylist(int playlistId, TrackDTO trackDTO) {
        String query = "INSERT INTO playlist_track (playlist_id, track_id, offlineavailable) VALUES (?,?,?)";
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, playlistId);
            statement.setInt(2, trackDTO.getId());
            statement.setBoolean(3, trackDTO.isOfflineAvailable());
            statement.execute();
        } catch (SQLException e) {
            throw new ResourceNotFoundException(e);
        }
    }

    @Override
    public void removeTrackFromPlaylist(int playlistId, int trackId) {
        String query = "DELETE FROM playlist_track WHERE playlist_id = ? AND track_id = ?";
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, playlistId);
            statement.setInt(2, trackId);
            statement.execute();
        } catch (SQLException e) {
            throw new ResourceNotFoundException(e);
        }
    }

    @Inject
    public void setConnectionFactory(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }
}
