package nl.mojiweb.joey.han.spotitube.service.authentication;

import nl.mojiweb.joey.han.spotitube.exception.PropertyFileMissingException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TokenProperties {

    private static final String TOKEN_PROPERTIES = "token.properties";

    private static Properties tokens;
    private String propertyFile;

    TokenProperties() {
        this(TOKEN_PROPERTIES);
    }

    TokenProperties(String fileLocation) {
        this.propertyFile = fileLocation;
    }

    private void saveTokens() {
        try {
            tokens.store(new FileOutputStream(propertyFile), null);
        } catch (IOException e) {
            throw new PropertyFileMissingException(e);
        }
    }

    private Properties getTokens() {
        if (tokens == null) {
            tokens = new Properties();
            try (InputStream stream = AuthenticationServiceImpl.class.getClassLoader().getResourceAsStream(propertyFile)) {
                tokens.load(stream);
            } catch (IOException | NullPointerException e) {
                throw new PropertyFileMissingException(e);
            }
        }
        return tokens;
    }

    void saveToken(String token, String user) {
        getTokens().setProperty(token, user);
        saveTokens();
    }

    void reset() {
        tokens = null;
    }

    String getUser(String token) {
        return ((String) getTokens().get(token));
    }
}
