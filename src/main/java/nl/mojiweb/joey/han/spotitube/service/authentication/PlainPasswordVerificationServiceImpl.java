package nl.mojiweb.joey.han.spotitube.service.authentication;

import nl.mojiweb.joey.han.spotitube.service.authentication.PasswordVerificationService;

import javax.enterprise.inject.Default;

@Default
public class PlainPasswordVerificationServiceImpl implements PasswordVerificationService {
    @Override
    public boolean validate(String password, String passwordToCompare) {
        //TODO: hash passwords
        return password.equals(passwordToCompare);
    }

    public String hashPassword(String passwordToHash) {
        return passwordToHash;
    }
}
