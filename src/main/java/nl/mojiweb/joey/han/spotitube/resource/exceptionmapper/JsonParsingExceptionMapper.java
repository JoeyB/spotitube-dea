package nl.mojiweb.joey.han.spotitube.resource.exceptionmapper;

import javax.json.stream.JsonParsingException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class JsonParsingExceptionMapper implements ExceptionMapper<JsonParsingException> {
    @Override
    public Response toResponse(JsonParsingException exception) {
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
