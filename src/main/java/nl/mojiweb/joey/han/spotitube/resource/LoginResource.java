package nl.mojiweb.joey.han.spotitube.resource;

import nl.mojiweb.joey.han.spotitube.resource.annotation.NoAuthentication;
import nl.mojiweb.joey.han.spotitube.service.authentication.AuthenticationService;
import nl.mojiweb.joey.han.spotitube.dto.LoginDTO;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/login")
public class LoginResource {

    private AuthenticationService authenticationService;

    @NoAuthentication
    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(LoginDTO loginDTO) {
        return Response.ok()
                .entity(authenticationService.login(loginDTO))
                .build();
    }

    @Inject
    public void setAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }
}
