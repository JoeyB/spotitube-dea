package nl.mojiweb.joey.han.spotitube.service.authentication;

import nl.mojiweb.joey.han.spotitube.dto.TokenDTO;
import nl.mojiweb.joey.han.spotitube.dto.UserDTO;

public interface TokenService {
    TokenDTO generateToken(UserDTO userDTO);
}
