package nl.mojiweb.joey.han.spotitube.resource;

import nl.mojiweb.joey.han.spotitube.exception.IdFormatException;
import nl.mojiweb.joey.han.spotitube.service.TrackService;
import nl.mojiweb.joey.han.spotitube.dto.TrackCollectionDTO;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/tracks")
public class TrackResource {

    private TrackService trackService;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTracks(
            @QueryParam("forPlaylist") String playListId) {
        //get tracks
        TrackCollectionDTO trackCollectionDTO;
        if (playListId == null || playListId.isEmpty()) {
            trackCollectionDTO = trackService.getAllTracks();
        } else {
            try {
                trackCollectionDTO = trackService.getTracksExcludingPlaylist(Integer.parseInt(playListId));
            } catch (NumberFormatException e) {
                throw new IdFormatException();
            }
        }
        //return response
        return Response.ok()
                .entity(trackCollectionDTO)
                .build();
    }

    @Inject
    public void setTrackService(TrackService trackService) {
        this.trackService = trackService;
    }
}
