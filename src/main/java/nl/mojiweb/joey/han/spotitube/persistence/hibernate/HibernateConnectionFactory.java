package nl.mojiweb.joey.han.spotitube.persistence.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.inject.Singleton;

@Singleton
public class HibernateConnectionFactory {

    private SessionFactory sessionFactory;

    public HibernateConnectionFactory() {
        buildSessionFactory();
    }

    private void buildSessionFactory() {
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }

    public Session getSession() {
        return sessionFactory.openSession();
    }
}
