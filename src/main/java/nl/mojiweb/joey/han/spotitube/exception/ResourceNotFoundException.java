package nl.mojiweb.joey.han.spotitube.exception;

public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(Exception cause) {
        super(cause);
    }

    public ResourceNotFoundException() {
    }
}
