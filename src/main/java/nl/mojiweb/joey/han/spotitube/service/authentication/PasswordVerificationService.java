package nl.mojiweb.joey.han.spotitube.service.authentication;

public interface PasswordVerificationService {
    boolean validate(String password, String passwordToValidate);

    String hashPassword(String passwordToHash);
}
