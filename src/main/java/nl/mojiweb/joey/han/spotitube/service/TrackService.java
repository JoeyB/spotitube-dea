package nl.mojiweb.joey.han.spotitube.service;

import nl.mojiweb.joey.han.spotitube.dto.TrackCollectionDTO;
import nl.mojiweb.joey.han.spotitube.dto.TrackDTO;

public interface TrackService {

    TrackCollectionDTO getAllTracks();

    TrackCollectionDTO getTracksByPlaylist(int playlistId);

    void removeTrackFromPlaylist(int playlistId, int trackId);

    void addTrackForPlaylist(int playlistId, TrackDTO trackDTO);

    TrackCollectionDTO getTracksExcludingPlaylist(int parseInt);
}
