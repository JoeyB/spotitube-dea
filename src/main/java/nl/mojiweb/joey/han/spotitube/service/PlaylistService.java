package nl.mojiweb.joey.han.spotitube.service;

import nl.mojiweb.joey.han.spotitube.dto.PlaylistCollectionDTO;
import nl.mojiweb.joey.han.spotitube.dto.PlaylistDTO;
import nl.mojiweb.joey.han.spotitube.dto.TrackCollectionDTO;

public interface PlaylistService {

    PlaylistCollectionDTO getAllPlaylists(String user);

    void deletePlaylist(int id);

    void addPlaylist(PlaylistDTO playlistDTO, String user);

    void updatePlaylist(int invalidId, PlaylistDTO playlistDTO);

    PlaylistDTO getPlaylist(int playlistId);
}
