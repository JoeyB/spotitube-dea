package nl.mojiweb.joey.han.spotitube.persistence.hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "playlist_track", schema = "spotitude", catalog = "")
@IdClass(PlaylistTrackEntityPK.class)
public class PlaylistTrackEntity {
    private int playlistId;
    private int trackId;
    private Byte offlineavailable;

    @Id
    @Column(name = "playlist_id")
    public int getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(int playlistId) {
        this.playlistId = playlistId;
    }

    @Id
    @Column(name = "track_id")
    public int getTrackId() {
        return trackId;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }

    @Basic
    @Column(name = "offlineavailable")
    public Byte getOfflineavailable() {
        return offlineavailable;
    }

    public void setOfflineavailable(Byte offlineavailable) {
        this.offlineavailable = offlineavailable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlaylistTrackEntity that = (PlaylistTrackEntity) o;
        return playlistId == that.playlistId &&
                trackId == that.trackId &&
                Objects.equals(offlineavailable, that.offlineavailable);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playlistId, trackId, offlineavailable);
    }
}
