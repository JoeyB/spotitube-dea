package nl.mojiweb.joey.han.spotitube.service;

import nl.mojiweb.joey.han.spotitube.dto.TrackCollectionDTO;
import nl.mojiweb.joey.han.spotitube.dto.TrackDTO;
import nl.mojiweb.joey.han.spotitube.persistence.TrackDAO;

import javax.inject.Inject;
import java.util.List;

public class TrackServiceImpl implements TrackService {

    private TrackDAO trackDAO;

    private PlaylistService playlistService;

    @Override
    public TrackCollectionDTO getAllTracks() {
        List<TrackDTO> tracks = trackDAO.getTracks();
        TrackCollectionDTO trackCollectionDTO = new TrackCollectionDTO();
        trackCollectionDTO.setTracks(tracks);
        return trackCollectionDTO;
    }

    @Override
    public TrackCollectionDTO getTracksByPlaylist(int playlistId) {
        playlistService.getPlaylist(playlistId);
        List<TrackDTO> tracks = trackDAO.getTracksByPlaylist(playlistId);
        TrackCollectionDTO trackCollectionDTO = new TrackCollectionDTO();
        trackCollectionDTO.setTracks(tracks);
        return trackCollectionDTO;
    }

    @Override
    public void removeTrackFromPlaylist(int playlistId, int trackId) {
        playlistService.getPlaylist(playlistId);
        trackDAO.removeTrackFromPlaylist(playlistId, trackId);
    }

    @Override
    public void addTrackForPlaylist(int playlistId, TrackDTO trackDTO) {
        playlistService.getPlaylist(playlistId);
        trackDAO.addTrackToPlaylist(playlistId, trackDTO);
    }

    @Override
    public TrackCollectionDTO getTracksExcludingPlaylist(int id) {
        List<TrackDTO> tracks = trackDAO.getTracksExcludingPlaylist(id);
        TrackCollectionDTO trackCollectionDTO = new TrackCollectionDTO();
        trackCollectionDTO.setTracks(tracks);
        return trackCollectionDTO;
    }

    @Inject
    public void setTrackDAO(TrackDAO trackDAO) {
        this.trackDAO = trackDAO;
    }

    @Inject
    public void setPlaylistService(PlaylistService playlistService) {
        this.playlistService = playlistService;
    }
}
