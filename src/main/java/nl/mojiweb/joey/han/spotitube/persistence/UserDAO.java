package nl.mojiweb.joey.han.spotitube.persistence;

import nl.mojiweb.joey.han.spotitube.dto.UserDTO;

public interface UserDAO {
    UserDTO getUserByUsername(String user);
}
