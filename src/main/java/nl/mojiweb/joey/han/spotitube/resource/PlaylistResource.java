package nl.mojiweb.joey.han.spotitube.resource;

import nl.mojiweb.joey.han.spotitube.dto.PlaylistCollectionDTO;
import nl.mojiweb.joey.han.spotitube.service.PlaylistService;
import nl.mojiweb.joey.han.spotitube.dto.PlaylistDTO;
import nl.mojiweb.joey.han.spotitube.dto.TrackDTO;
import nl.mojiweb.joey.han.spotitube.service.TrackService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/playlists")
public class PlaylistResource {

    private PlaylistService playlistService;

    private SecurityContext securityContext;
    private TrackService trackService;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlaylistCollection() {
        String user = securityContext.getUserPrincipal().getName();
        PlaylistCollectionDTO allPlaylists = playlistService.getAllPlaylists(user);
        return Response.ok()
                .entity(allPlaylists)
                .build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePlaylist(
            @PathParam("id") int id) {
        playlistService.deletePlaylist(id);
        return Response.ok()
                .entity(playlistService.getAllPlaylists(securityContext.getUserPrincipal().getName()))
                .build();
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addPlaylist(
            PlaylistDTO playlistDTO) {
        String user = securityContext.getUserPrincipal().getName();
        playlistService.addPlaylist(playlistDTO, user);
        return Response.status(Response.Status.CREATED)
                .entity(playlistService.getAllPlaylists(user))
                .build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editPlaylist(
            @PathParam("id") int id,
            PlaylistDTO playlistDTO) {
        playlistService.updatePlaylist(id, playlistDTO);
        return Response.ok()
                .entity(playlistService.getAllPlaylists(securityContext.getUserPrincipal().getName()))
                .build();
    }

    @GET
    @Path("/{id}/tracks")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTracksForPlaylist(
            @PathParam("id") int playlistId) {
        return Response.ok()
                .entity(trackService.getTracksByPlaylist(playlistId))
                .build();
    }

    @DELETE
    @Path("/{playlist-id}/tracks/{track-id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteTrackForPlaylist(
            @PathParam("playlist-id") int playlistId,
            @PathParam("track-id") int trackId) {
        trackService.removeTrackFromPlaylist(playlistId, trackId);
        return Response.ok()
                .entity(trackService.getTracksByPlaylist(playlistId))
                .build();
    }

    @POST
    @Path("/{id}/tracks")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addTrackForPlaylist(
            @PathParam("id") int playlistId,
            TrackDTO trackDTO) {
        trackService.addTrackForPlaylist(playlistId, trackDTO);
        return Response.status(Response.Status.CREATED)
                .entity(trackService.getTracksByPlaylist(playlistId))
                .build();
    }

    @Inject
    public void setPlaylistService(PlaylistService playlistService) {
        this.playlistService = playlistService;
    }

    @Context
    public void setSecurityContext(SecurityContext context) {
        this.securityContext = context;
    }

    @Inject
    public void setTrackService(TrackService trackService) {
        this.trackService = trackService;
    }
}
