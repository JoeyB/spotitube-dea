package nl.mojiweb.joey.han.spotitube.resource.exceptionmapper;

import nl.mojiweb.joey.han.spotitube.exception.InvalidIdException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class InvalidIdExceptionMapper implements ExceptionMapper<InvalidIdException> {

    @Override
    public Response toResponse(InvalidIdException exception) {
        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
