package nl.mojiweb.joey.han.spotitube.persistence.dbconnection;

import org.apache.commons.dbcp.BasicDataSource;

import javax.persistence.PersistenceException;
import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionFactory {

    Connection getConnection() throws PersistenceException;
}
