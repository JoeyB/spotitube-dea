package nl.mojiweb.joey.han.spotitube.persistence;

import nl.mojiweb.joey.han.spotitube.dto.UserDTO;
import nl.mojiweb.joey.han.spotitube.persistence.dbconnection.ConnectionFactory;
import nl.mojiweb.joey.han.spotitube.exception.UserNotFoundException;

import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.persistence.PersistenceException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Default
public class UserDAOImpl implements UserDAO {

    private static final String SELECT_USER = "SELECT * FROM `user` WHERE username = ?";
    private ConnectionFactory connectionFactory;


    @Override
    public UserDTO getUserByUsername(String user) {
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_USER);
            statement.setString(1, user);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                UserDTO userDTO = new UserDTO();
                userDTO.setName(resultSet.getString("name"));
                userDTO.setPassword(resultSet.getString("password"));
                userDTO.setUsername(resultSet.getString("username"));
                return userDTO;
            }
        } catch (SQLException e) {
            throw new PersistenceException(e);
        }
        throw new UserNotFoundException();
    }

    @Inject
    public void setConnectionFactory(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }


}
