package nl.mojiweb.joey.han.spotitube.dto;

import java.util.List;

public class PlaylistCollectionDTO {
    private List<PlaylistDTO> playlists;
    private int length;

    public void setPlaylists(List<PlaylistDTO> playlists) {
        this.playlists = playlists;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public List<PlaylistDTO> getPlaylists() {
        return playlists;
    }
}
