package nl.mojiweb.joey.han.spotitube.dto;

public class TokenDTO {
    private String user;
    private String token;

    public TokenDTO() {
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
