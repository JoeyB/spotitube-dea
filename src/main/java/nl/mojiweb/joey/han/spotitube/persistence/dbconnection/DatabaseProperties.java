package nl.mojiweb.joey.han.spotitube.persistence.dbconnection;

import nl.mojiweb.joey.han.spotitube.exception.PropertyFileMissingException;

import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Singleton
public class DatabaseProperties {

    private String password;
    private String username;
    private String connectionString;
    private String driver;

    public DatabaseProperties() {
        loadProperties();
    }

    private void loadProperties() {
        Properties properties = new Properties();
        try (InputStream stream = getClass().getClassLoader().getResourceAsStream("database.properties")) {
            properties.load(stream);
            this.password = properties.getProperty("db.pass");
            this.username = properties.getProperty("db.user");
            this.driver = properties.getProperty("db.driver");
            this.connectionString = properties.getProperty("db.connectionstring")
                    .replace("{{host}}", properties.getProperty("db.host"))
                    .replace("{{dbname}}", properties.getProperty("db.dbname"));
        } catch (IOException e) {
            throw new PropertyFileMissingException(e);
        }
    }

    public String getConnectionString() {
        return connectionString;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public String getDriver() {
        return driver;
    }
}
