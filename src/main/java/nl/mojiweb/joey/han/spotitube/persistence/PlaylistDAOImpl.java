package nl.mojiweb.joey.han.spotitube.persistence;

import nl.mojiweb.joey.han.spotitube.dto.PlaylistDTO;
import nl.mojiweb.joey.han.spotitube.exception.ResourceNotFoundException;
import nl.mojiweb.joey.han.spotitube.persistence.dbconnection.ConnectionFactory;

import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.persistence.PersistenceException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Default
public class PlaylistDAOImpl implements PlaylistDAO {

    private ConnectionFactory connectionFactory;

    public List<PlaylistDTO> getPlaylists(String user) {
        List<PlaylistDTO> playlistDTOS = new ArrayList<>();
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM playlist");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                PlaylistDTO playlistDTO = new PlaylistDTO();
                playlistDTO.setId(resultSet.getInt("id"));
                playlistDTO.setName(resultSet.getString("name"));
                playlistDTO.setOwner(user.equals(resultSet.getString("user")));
                playlistDTO.setTracks(new ArrayList<>());
                playlistDTOS.add(playlistDTO);
            }
        } catch (SQLException e) {
            throw new PersistenceException(e);
        }
        return playlistDTOS;
    }

    @Override
    public int getTotalPlaylistDuration() {
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("SELECT SUM(t.duration) FROM playlist_track JOIN track t on playlist_track.track_id = t.id");
            ResultSet resultSet = statement.executeQuery();
            int result = 0;
            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }
            return result;
        } catch (SQLException e) {
            throw new PersistenceException(e);
        }
    }

    @Override
    public void addPlaylist(PlaylistDTO playlistDTO, String user) {
        String query = "INSERT INTO `playlist` (`user`, `name`) VALUES (?, ?)";

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, user);
            statement.setString(2, playlistDTO.getName());
            statement.execute();
            if (statement.getUpdateCount() == 0) throw new PersistenceException();
        } catch (SQLException e) {
            throw new PersistenceException(e);
        }
    }

    @Override
    public void deletePlaylist(int id) {
        String query = "DELETE FROM `playlist` WHERE  `id`=?";
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            throw new PersistenceException(e);
        }
    }

    @Override
    public void update(PlaylistDTO playlistDTO) {
        String query = "UPDATE `playlist` SET `name`=? WHERE `id`=?";

        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, playlistDTO.getName());
            statement.setInt(2, playlistDTO.getId());
            statement.execute();
        } catch (SQLException e) {
            throw new PersistenceException(e);
        }
    }

    @Override
    public PlaylistDTO getPlaylist(int playlistId) {
        String query = "SELECT * FROM playlist WHERE id = ?";
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, playlistId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                PlaylistDTO playlistDTO = new PlaylistDTO();
                playlistDTO.setId(resultSet.getInt("id"));
                playlistDTO.setName(resultSet.getString("name"));
                playlistDTO.setTracks(new ArrayList<>());
                playlistDTO.setOwner(true);
                return playlistDTO;
            }
        } catch (SQLException e) {
            throw new PersistenceException(e);
        }
        throw new ResourceNotFoundException();
    }

    @Inject
    public void setConnectionFactory(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }
}
