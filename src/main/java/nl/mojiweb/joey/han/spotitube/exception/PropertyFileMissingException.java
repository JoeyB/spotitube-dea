package nl.mojiweb.joey.han.spotitube.exception;

public class PropertyFileMissingException extends RuntimeException {

    public PropertyFileMissingException(Throwable cause) {
        super(cause);
    }
}
