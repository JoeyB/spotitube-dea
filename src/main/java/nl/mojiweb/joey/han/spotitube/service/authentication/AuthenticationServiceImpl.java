package nl.mojiweb.joey.han.spotitube.service.authentication;

import nl.mojiweb.joey.han.spotitube.dto.LoginDTO;
import nl.mojiweb.joey.han.spotitube.dto.TokenDTO;
import nl.mojiweb.joey.han.spotitube.dto.UserDTO;
import nl.mojiweb.joey.han.spotitube.persistence.UserDAO;
import nl.mojiweb.joey.han.spotitube.exception.UserNotFoundException;
import nl.mojiweb.joey.han.spotitube.exception.InvalidCredentialsException;
import nl.mojiweb.joey.han.spotitube.exception.InvalidTokenException;

import javax.enterprise.inject.Default;
import javax.inject.Inject;

@Default
public class AuthenticationServiceImpl implements AuthenticationService {

    private UserDAO userDAO;
    private TokenService tokenService;
    private PasswordVerificationService passwordVerificationService;
    private TokenProperties tokenProperties;

    @Override
    public TokenDTO login(LoginDTO loginDTO) {
        try {
            UserDTO user = userDAO.getUserByUsername(loginDTO.getUser());
            if (passwordVerificationService.validate(user.getPassword(), loginDTO.getPassword())) {
                TokenDTO tokenDTO = tokenService.generateToken(user);
                tokenProperties.saveToken(tokenDTO.getToken(), loginDTO.getUser());
                return tokenDTO;
            }
        } catch (UserNotFoundException e) {
            e.printStackTrace();
        }
        throw new InvalidCredentialsException();
    }

    @Override
    public String authenticate(String token) {
        String user = tokenProperties.getUser(token);
        if (user == null) {
            throw new InvalidTokenException();
        }
        return user;
    }

    @Inject
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Inject
    public void setTokenService(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @Inject
    public void setPasswordVerificationService(PasswordVerificationService passwordVerificationService) {
        this.passwordVerificationService = passwordVerificationService;
    }

    @Inject
    public void setTokenProperties(TokenProperties tokenProperties) {
        this.tokenProperties = tokenProperties;
    }
}
