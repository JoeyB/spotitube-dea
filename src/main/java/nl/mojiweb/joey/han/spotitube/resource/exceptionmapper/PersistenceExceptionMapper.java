package nl.mojiweb.joey.han.spotitube.resource.exceptionmapper;

import javax.persistence.PersistenceException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class PersistenceExceptionMapper implements ExceptionMapper<PersistenceException> {
    @Override
    public Response toResponse(PersistenceException exception) {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
}
