package nl.mojiweb.joey.han.spotitube.persistence.hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "playlist", schema = "spotitude")
public class PlaylistEntity {
    private int id;
    private String name;
    private UserEntity user;
    private List<TrackEntity> tracks;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne()
    @JoinColumn(name="user")
    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    @ManyToMany
    @JoinTable(
            name="playlist_track",
            joinColumns = { @JoinColumn(name="playlist_id")},
            inverseJoinColumns = { @JoinColumn(name="track_id") }
    )
    public List<TrackEntity> getTracks() {
        return this.tracks;
    }

    public void setTracks(List<TrackEntity> tracks) {
        this.tracks = tracks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlaylistEntity that = (PlaylistEntity) o;
        return id == that.id &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
