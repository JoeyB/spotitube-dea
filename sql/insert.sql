-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.38-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table spotitude.playlist: ~2 rows (approximately)
/*!40000 ALTER TABLE `playlist` DISABLE KEYS */;
INSERT INTO `playlist` (`id`, `user`, `name`) VALUES
	(3, 'joey', 'Main Playlist'),
	(4, 'joey', 'Nicole\'s shizzle');
/*!40000 ALTER TABLE `playlist` ENABLE KEYS */;

-- Dumping data for table spotitude.playlist_track: ~0 rows (approximately)
/*!40000 ALTER TABLE `playlist_track` DISABLE KEYS */;
INSERT INTO `playlist_track` (`playlist_id`, `track_id`, `offlineavailable`) VALUES
	(3, 55, 1),
	(4, 53, 1);
/*!40000 ALTER TABLE `playlist_track` ENABLE KEYS */;

-- Dumping data for table spotitude.track: ~51 rows (approximately)
/*!40000 ALTER TABLE `track` DISABLE KEYS */;
INSERT INTO `track` (`id`, `title`, `performer`, `duration`, `album`, `playcount`, `date`, `description`) VALUES
	(1, 'Highway to hell', 'AC/DC', 220, NULL, 5, '03-15-1980', 'Good record'),
	(52, 'For You', 'Kelly Chang', 245, NULL, 852, '03-03-2020', 'turpis nec mauris blandit mattis. Cras'),
	(53, 'CountryRoad', 'Harper Pierce', 125, NULL, 933, '03-24-2019', 'tellus non magna. Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien, cursus'),
	(54, 'Hell', 'Jena Terry', 212, NULL, 375, '02-10-2020', 'est ac mattis semper, dui lectus rutrum urna,'),
	(55, 'Hell', 'Jennifer Hess', 297, NULL, 529, '10-02-2020', 'nec ante blandit viverra. Donec'),
	(56, 'For You', 'Debra Gill', 259, NULL, 106, '10-17-2020', 'aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos.'),
	(57, 'CountryRoad', 'Rudyard Kemp', 255, NULL, 592, '03-27-2020', 'Quisque ornare tortor at risus. Nunc'),
	(58, 'RocknRoll', 'Yen Boyd', 295, NULL, 603, '01-10-2019', 'Aenean euismod mauris eu elit. Nulla facilisi. Sed neque. Sed eget lacus. Mauris non dui nec urna suscipit'),
	(59, 'For You', 'Selma Davenport', 132, NULL, 887, '12-26-2018', 'et magnis dis parturient montes, nascetur ridiculus mus. Donec dignissim magna a tortor. Nunc commodo auctor velit. Aliquam'),
	(60, 'Hell', 'Jermaine Howe', 134, NULL, 480, '11-06-2018', 'neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper, velit in aliquet lobortis, nisi'),
	(61, 'For You', 'Halla Patel', 205, NULL, 457, '03-05-2019', 'Donec vitae erat vel pede blandit congue. In scelerisque scelerisque dui. Suspendisse ac metus vitae velit egestas lacinia. Sed congue,'),
	(62, 'Love', 'Marshall Park', 291, NULL, 442, '06-16-2019', 'risus quis diam luctus lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per'),
	(63, 'RocknRoll', 'Cadman Roy', 294, NULL, 647, '11-06-2018', 'Etiam ligula tortor, dictum eu, placerat eget, venenatis a,'),
	(64, 'CountryRoad', 'Ryder Good', 168, NULL, 530, '03-07-2019', 'tempus eu, ligula. Aenean euismod mauris eu elit. Nulla facilisi. Sed'),
	(65, 'CountryRoad', 'Ezekiel Pena', 221, NULL, 775, '03-27-2019', 'vel, venenatis vel, faucibus id, libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non, lacinia at, iaculis'),
	(66, 'Storm', 'Ella Lang', 130, NULL, 260, '11-06-2018', 'feugiat non, lobortis quis, pede. Suspendisse dui. Fusce diam nunc, ullamcorper eu, euismod ac,'),
	(67, 'RocknRoll', 'Fatima Rivers', 167, NULL, 648, '03-23-2020', 'consectetuer adipiscing elit. Etiam laoreet, libero et tristique pellentesque, tellus sem mollis dui, in'),
	(68, 'Storm', 'Cleo Mann', 201, NULL, 586, '07-20-2020', 'a, malesuada id, erat. Etiam vestibulum'),
	(69, 'CountryRoad', 'Davis Serrano', 192, NULL, 913, '12-28-2019', 'et, magna. Praesent interdum ligula eu enim. Etiam'),
	(70, 'For You', 'Alec Moreno', 232, NULL, 863, '03-10-2019', 'vitae purus gravida sagittis. Duis gravida. Praesent'),
	(71, 'Hell', 'Dylan Mcclure', 161, 'Lycos', NULL, NULL, NULL),
	(72, 'For You', 'Myra Atkinson', 224, 'Altavista', NULL, NULL, NULL),
	(73, 'RocknRoll', 'Yuli Deleon', 207, 'Apple Systems', NULL, NULL, NULL),
	(74, 'For You', 'Kim Fox', 179, 'Sibelius', NULL, NULL, NULL),
	(75, 'RocknRoll', 'Chester Walker', 284, 'Cakewalk', NULL, NULL, NULL),
	(76, 'For You', 'Macey Morrow', 292, 'Sibelius', NULL, NULL, NULL),
	(77, 'RocknRoll', 'Karly Casey', 180, 'Microsoft', NULL, NULL, NULL),
	(78, 'Hell', 'Zachary Berg', 160, 'Altavista', NULL, NULL, NULL),
	(79, 'For You', 'Thane Torres', 222, 'Google', NULL, NULL, NULL),
	(80, 'RocknRoll', 'Iris Ortega', 248, 'Lavasoft', NULL, NULL, NULL),
	(81, 'For You', 'Bo Mccullough', 238, 'Borland', NULL, NULL, NULL),
	(82, 'RocknRoll', 'Ira Clay', 269, 'Macromedia', NULL, NULL, NULL),
	(83, 'Storm', 'Joel Reid', 237, 'Yahoo', NULL, NULL, NULL),
	(84, 'RocknRoll', 'Alfreda Mcmahon', 286, 'Lycos', NULL, NULL, NULL),
	(85, 'Hell', 'Paki Walker', 134, 'Borland', NULL, NULL, NULL),
	(86, 'Love', 'Kamal Shields', 225, 'Yahoo', NULL, NULL, NULL),
	(87, 'Love', 'Hilel Espinoza', 158, 'Lycos', NULL, NULL, NULL),
	(88, 'Love', 'Bert Bowen', 225, 'Microsoft', NULL, NULL, NULL),
	(89, 'Hell', 'Mariko Hayden', 170, 'Chami', NULL, NULL, NULL),
	(90, 'Storm', 'Hillary Burt', 255, 'Cakewalk', NULL, NULL, NULL),
	(91, 'Love', 'Alexander Black', 201, 'Macromedia', NULL, NULL, NULL),
	(92, 'CountryRoad', 'Kaseem Walker', 207, 'Lavasoft', NULL, NULL, NULL),
	(93, 'Love', 'Leonard Pugh', 127, 'Cakewalk', NULL, NULL, NULL),
	(94, 'Love', 'Amena Kirk', 294, 'Borland', NULL, NULL, NULL),
	(95, 'Love', 'Cade Rutledge', 221, 'Sibelius', NULL, NULL, NULL),
	(96, 'CountryRoad', 'Morgan Ayers', 185, 'Chami', NULL, NULL, NULL),
	(97, 'Hell', 'Kerry Meyers', 178, 'Lycos', NULL, NULL, NULL),
	(98, 'For You', 'Renee Ball', 286, 'Adobe', NULL, NULL, NULL),
	(99, 'Love', 'Ferris Henry', 289, 'Borland', NULL, NULL, NULL),
	(100, 'CountryRoad', 'Nicholas Key', 291, 'Sibelius', NULL, NULL, NULL),
	(101, 'For You', 'Abbot Albert', 266, 'Sibelius', NULL, NULL, NULL);
/*!40000 ALTER TABLE `track` ENABLE KEYS */;

-- Dumping data for table spotitude.user: ~1 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`username`, `password`, `name`) VALUES
	('joey', 'test123', 'Joey Boerwinkel');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
