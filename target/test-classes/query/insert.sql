INSERT INTO user (username, password, name)
VALUES ('testUsername', 'testPassword', 'testName');

INSERT INTO playlist (id, user, name)
VALUES (1, 'testUsername', 'testPlaylist1'),
       (2, 'testUsername', 'testPlaylist2');

INSERT INTO track (id, title, performer, duration, album, playcount, date, description)
VALUES (1, 'testTitle1', 'testPerformer1', 10, 'testAlbum1', NULL, '01-01-1970', 'testDescription1'),
       (2, 'testTitle2', 'testPerformer2', 10, 'testAlbum2', NULL, '02-01-1970', 'testDescription2'),
       (3, 'testTitle3', 'testPerformer3', 10, 'testAlbum3', NULL, '03-01-1970', 'testDescription3');

INSERT INTO playlist_track (playlist_id, track_id, offlineavailable)
VALUES (1, 1, 1),
       (1, 2, 1),
       (2, 1, 1),
       (2, 3, 1),
       (2, 2, 1)
