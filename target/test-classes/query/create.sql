-- Data exporting was unselected.
-- Dumping structure for table spotitude.user
CREATE TABLE IF NOT EXISTS `user`
(
    `username` varchar(50) NOT NULL,
    `password` varchar(255) DEFAULT NULL,
    `name`     varchar(255) DEFAULT NULL,
    PRIMARY KEY (`username`)
);

-- Dumping structure for table spotitude.playlist
CREATE TABLE IF NOT EXISTS `playlist`
(
    `id`   int(11) NOT NULL AUTO_INCREMENT,
    `user` varchar(50)  DEFAULT NULL,
    `name` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_user` (`user`),
    CONSTRAINT `fk_user` FOREIGN KEY (`user`) REFERENCES `user` (`username`) ON UPDATE CASCADE ON DELETE CASCADE
);

-- Data exporting was unselected.
-- Dumping structure for table spotitude.track
CREATE TABLE IF NOT EXISTS `track`
(
    `id`          int(11)      NOT NULL AUTO_INCREMENT,
    `title`       varchar(255) NOT NULL,
    `performer`   varchar(255) NOT NULL,
    `duration`    int(11)      NOT NULL,
    `album`       varchar(255) DEFAULT NULL,
    `playcount`   int(11)      DEFAULT NULL,
    `date`        varchar(255) DEFAULT NULL,
    `description` text,
    PRIMARY KEY (`id`)
);

-- Data exporting was unselected.
-- Dumping structure for table spotitude.playlist_track
CREATE TABLE IF NOT EXISTS `playlist_track`
(
    `playlist_id`      int(11) NOT NULL,
    `track_id`         int(11) NOT NULL,
    `offlineavailable` tinyint(1) DEFAULT NULL,
    PRIMARY KEY (`playlist_id`, `track_id`),
    KEY `FK_track` (`track_id`),
    CONSTRAINT `FK_playlist` FOREIGN KEY (`playlist_id`) REFERENCES `playlist` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_track` FOREIGN KEY (`track_id`) REFERENCES `track` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);